/*
 Navicat Premium Data Transfer

 Source Server         : 联想服务器
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : xpocker.numberline.top:3306
 Source Schema         : xpocker

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 22/07/2021 11:07:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cr_room_info
-- ----------------------------
DROP TABLE IF EXISTS `cr_room_info`;
CREATE TABLE `cr_room_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `room_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `room_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `room_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `create_str` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cr_room_info
-- ----------------------------

-- ----------------------------
-- Table structure for e_room_history
-- ----------------------------
DROP TABLE IF EXISTS `e_room_history`;
CREATE TABLE `e_room_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_numbers` int(11) NULL DEFAULT NULL,
  `start_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `room_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of e_room_history
-- ----------------------------

-- ----------------------------
-- Table structure for e_room_history_fraction
-- ----------------------------
DROP TABLE IF EXISTS `e_room_history_fraction`;
CREATE TABLE `e_room_history_fraction`  (
  `history_id` int(11) NOT NULL,
  `user_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fraction_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `fraction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of e_room_history_fraction
-- ----------------------------

-- ----------------------------
-- Table structure for e_room_user
-- ----------------------------
DROP TABLE IF EXISTS `e_room_user`;
CREATE TABLE `e_room_user`  (
  `room_id` int(11) NOT NULL COMMENT 'room表外键',
  `user_id` int(11) NULL DEFAULT NULL,
  `user_type` int(11) NULL DEFAULT 0 COMMENT '玩家类型 有可能是观战'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of e_room_user
-- ----------------------------
INSERT INTO `e_room_user` VALUES (1, 888890, 0);
INSERT INTO `e_room_user` VALUES (1, 888891, 0);
INSERT INTO `e_room_user` VALUES (2, 888890, 0);
INSERT INTO `e_room_user` VALUES (2, 888891, 0);
INSERT INTO `e_room_user` VALUES (8, 888888, 0);
INSERT INTO `e_room_user` VALUES (8, 888891, 0);
INSERT INTO `e_room_user` VALUES (9, 888888, 0);
INSERT INTO `e_room_user` VALUES (9, 888891, 0);
INSERT INTO `e_room_user` VALUES (46, 888888, 0);
INSERT INTO `e_room_user` VALUES (46, 888904, 0);
INSERT INTO `e_room_user` VALUES (47, 888888, 0);
INSERT INTO `e_room_user` VALUES (47, 888904, 0);
INSERT INTO `e_room_user` VALUES (77, 888888, 0);
INSERT INTO `e_room_user` VALUES (77, 888891, 0);
INSERT INTO `e_room_user` VALUES (90, 888888, 0);
INSERT INTO `e_room_user` VALUES (90, 888889, 0);
INSERT INTO `e_room_user` VALUES (93, 888888, 0);
INSERT INTO `e_room_user` VALUES (93, 888891, 0);
INSERT INTO `e_room_user` VALUES (95, 888888, 0);
INSERT INTO `e_room_user` VALUES (95, 888889, 0);
INSERT INTO `e_room_user` VALUES (104, 888888, 0);
INSERT INTO `e_room_user` VALUES (104, 888904, 0);
INSERT INTO `e_room_user` VALUES (131, 888888, 0);
INSERT INTO `e_room_user` VALUES (131, 888904, 0);
INSERT INTO `e_room_user` VALUES (166, 888888, 0);
INSERT INTO `e_room_user` VALUES (166, 888904, 0);
INSERT INTO `e_room_user` VALUES (190, 888888, 0);
INSERT INTO `e_room_user` VALUES (190, 888889, 0);
INSERT INTO `e_room_user` VALUES (232, 888888, 0);
INSERT INTO `e_room_user` VALUES (232, 888904, 0);
INSERT INTO `e_room_user` VALUES (289, 888888, 0);
INSERT INTO `e_room_user` VALUES (289, 888889, 0);
INSERT INTO `e_room_user` VALUES (290, 888888, 0);
INSERT INTO `e_room_user` VALUES (290, 888889, 0);
INSERT INTO `e_room_user` VALUES (292, 888888, 0);
INSERT INTO `e_room_user` VALUES (292, 888889, 0);
INSERT INTO `e_room_user` VALUES (293, 888888, 0);
INSERT INTO `e_room_user` VALUES (293, 888889, 0);
INSERT INTO `e_room_user` VALUES (294, 888888, 0);
INSERT INTO `e_room_user` VALUES (294, 888889, 0);
INSERT INTO `e_room_user` VALUES (295, 888888, 0);
INSERT INTO `e_room_user` VALUES (295, 888891, 0);
INSERT INTO `e_room_user` VALUES (296, 888888, 0);
INSERT INTO `e_room_user` VALUES (296, 888891, 0);
INSERT INTO `e_room_user` VALUES (351, 888888, 0);
INSERT INTO `e_room_user` VALUES (351, 888889, 0);
INSERT INTO `e_room_user` VALUES (352, 888888, 0);
INSERT INTO `e_room_user` VALUES (352, 888904, 0);
INSERT INTO `e_room_user` VALUES (366, 888888, 0);
INSERT INTO `e_room_user` VALUES (366, 888890, 0);
INSERT INTO `e_room_user` VALUES (366, 888891, 0);
INSERT INTO `e_room_user` VALUES (366, 888889, 0);
INSERT INTO `e_room_user` VALUES (367, 888888, 0);
INSERT INTO `e_room_user` VALUES (367, 888889, 0);
INSERT INTO `e_room_user` VALUES (371, 888888, 0);
INSERT INTO `e_room_user` VALUES (371, 888890, 0);
INSERT INTO `e_room_user` VALUES (371, 888891, 0);
INSERT INTO `e_room_user` VALUES (371, 888889, 0);
INSERT INTO `e_room_user` VALUES (389, 888888, 0);
INSERT INTO `e_room_user` VALUES (389, 888889, 0);
INSERT INTO `e_room_user` VALUES (392, 888888, 0);
INSERT INTO `e_room_user` VALUES (392, 888908, 0);
INSERT INTO `e_room_user` VALUES (406, 888888, 0);
INSERT INTO `e_room_user` VALUES (406, 888890, 0);
INSERT INTO `e_room_user` VALUES (408, 888888, 0);
INSERT INTO `e_room_user` VALUES (408, 888890, 0);
INSERT INTO `e_room_user` VALUES (426, 888888, 0);
INSERT INTO `e_room_user` VALUES (426, 888907, 0);
INSERT INTO `e_room_user` VALUES (427, 888888, 0);
INSERT INTO `e_room_user` VALUES (427, 888907, 0);
INSERT INTO `e_room_user` VALUES (430, 888888, 0);
INSERT INTO `e_room_user` VALUES (430, 888909, 0);
INSERT INTO `e_room_user` VALUES (430, 888906, 0);
INSERT INTO `e_room_user` VALUES (430, 888907, 0);
INSERT INTO `e_room_user` VALUES (441, 888888, 0);
INSERT INTO `e_room_user` VALUES (441, 888890, 0);
INSERT INTO `e_room_user` VALUES (442, 888888, 0);
INSERT INTO `e_room_user` VALUES (442, 888890, 0);
INSERT INTO `e_room_user` VALUES (442, 888891, 0);
INSERT INTO `e_room_user` VALUES (442, 888889, 0);
INSERT INTO `e_room_user` VALUES (443, 888888, 0);
INSERT INTO `e_room_user` VALUES (443, 888890, 0);
INSERT INTO `e_room_user` VALUES (443, 888891, 0);
INSERT INTO `e_room_user` VALUES (443, 888889, 0);
INSERT INTO `e_room_user` VALUES (444, 888888, 0);
INSERT INTO `e_room_user` VALUES (444, 888890, 0);
INSERT INTO `e_room_user` VALUES (445, 888888, 0);
INSERT INTO `e_room_user` VALUES (445, 888890, 0);
INSERT INTO `e_room_user` VALUES (447, 888888, 0);
INSERT INTO `e_room_user` VALUES (447, 888890, 0);
INSERT INTO `e_room_user` VALUES (448, 888888, 0);
INSERT INTO `e_room_user` VALUES (448, 888890, 0);
INSERT INTO `e_room_user` VALUES (449, 888888, 0);
INSERT INTO `e_room_user` VALUES (449, 888890, 0);
INSERT INTO `e_room_user` VALUES (449, 888891, 0);
INSERT INTO `e_room_user` VALUES (449, 888889, 0);
INSERT INTO `e_room_user` VALUES (451, 888888, 0);
INSERT INTO `e_room_user` VALUES (451, 888909, 0);
INSERT INTO `e_room_user` VALUES (451, 888906, 0);
INSERT INTO `e_room_user` VALUES (451, 888907, 0);
INSERT INTO `e_room_user` VALUES (452, 888888, 0);
INSERT INTO `e_room_user` VALUES (452, 888909, 0);
INSERT INTO `e_room_user` VALUES (452, 888906, 0);
INSERT INTO `e_room_user` VALUES (452, 888907, 0);
INSERT INTO `e_room_user` VALUES (474, 888888, 0);
INSERT INTO `e_room_user` VALUES (474, 888906, 0);
INSERT INTO `e_room_user` VALUES (474, 888911, 0);
INSERT INTO `e_room_user` VALUES (474, 888905, 0);
INSERT INTO `e_room_user` VALUES (484, 888888, 0);
INSERT INTO `e_room_user` VALUES (484, 888890, 0);
INSERT INTO `e_room_user` VALUES (486, 888888, 0);
INSERT INTO `e_room_user` VALUES (486, 888890, 0);
INSERT INTO `e_room_user` VALUES (487, 888888, 0);
INSERT INTO `e_room_user` VALUES (487, 888909, 0);
INSERT INTO `e_room_user` VALUES (487, 888889, 0);
INSERT INTO `e_room_user` VALUES (487, 888906, 0);
INSERT INTO `e_room_user` VALUES (494, 888888, 0);
INSERT INTO `e_room_user` VALUES (494, 888891, 0);
INSERT INTO `e_room_user` VALUES (504, 888888, 0);
INSERT INTO `e_room_user` VALUES (504, 888891, 0);
INSERT INTO `e_room_user` VALUES (519, 888888, 0);
INSERT INTO `e_room_user` VALUES (519, 888891, 0);
INSERT INTO `e_room_user` VALUES (533, 888888, 0);
INSERT INTO `e_room_user` VALUES (533, 888890, 0);
INSERT INTO `e_room_user` VALUES (533, 888891, 0);
INSERT INTO `e_room_user` VALUES (533, 888889, 0);
INSERT INTO `e_room_user` VALUES (570, 888888, 0);
INSERT INTO `e_room_user` VALUES (570, 888890, 0);

-- ----------------------------
-- Table structure for mm_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `mm_dictionary`;
CREATE TABLE `mm_dictionary`  (
  `id` int(11) NOT NULL,
  `dict_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典值名称',
  `dict_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '字典值老爹',
  `create_date` datetime(0) NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mm_dictionary
-- ----------------------------
INSERT INTO `mm_dictionary` VALUES (1, 'ROOM_TYPE', '房间类型', NULL, '2021-06-04 11:52:35', '万杰', '2021-06-04 11:52:35', '万杰');

-- ----------------------------
-- Table structure for mm_dictionary_data
-- ----------------------------
DROP TABLE IF EXISTS `mm_dictionary_data`;
CREATE TABLE `mm_dictionary_data`  (
  `id` int(11) NOT NULL,
  `dict_data_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_data_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_id` int(11) NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mm_dictionary_data
-- ----------------------------
INSERT INTO `mm_dictionary_data` VALUES (1, '万州麻将', 'WZ_MAHJONG', 1, '2021-06-04 11:54:05', 'wj', '2021-06-04 11:54:05', 'wj');

-- ----------------------------
-- Table structure for mm_tables
-- ----------------------------
DROP TABLE IF EXISTS `mm_tables`;
CREATE TABLE `mm_tables`  (
  `id` int(11) NOT NULL,
  `table_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `updater` datetime(0) NULL DEFAULT NULL,
  `remarker` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mm_tables
-- ----------------------------

-- ----------------------------
-- Table structure for mm_tables_column
-- ----------------------------
DROP TABLE IF EXISTS `mm_tables_column`;
CREATE TABLE `mm_tables_column`  (
  `id` int(11) NOT NULL,
  `column_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `column_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `updater` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dictionary_id` int(11) NULL DEFAULT NULL COMMENT '字典值id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of mm_tables_column
-- ----------------------------

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `pid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES (1, '正常访问', 'ORDINARY', '2021-04-22 14:29:36', 1);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `data_scope` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '普通用户', '普通麻将用户', '2021-04-22 14:28:20', '普通用户', '1');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `permission_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `age` int(11) NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `portrait_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `version` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `updater` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `creater` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 888912 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (888888, 'wanjie', 'wanjie', 24, '18623234493', 'xplay', 'pFdygHjwXVA4pG8Cy9HZYedrYVSJ/IjiBvlOb/OEojrJJ0pxjGsR6L4u26xx8bOVF+Xz9f7lwHDcZzhcYZdYfkZXmixhgPb59nvFbMnXEmQvhSNzl/AX805fjFIBTBAfkbIhzF5DMJS80B1TfhN1x3tDJZDIeL4inaUsIzj22gYxkTUCCAYjFYrskpPaEf6kk3fON+QZWr0wsJPjpF1PLJSk73IasfLdSmf713PP/rhcq4G894te5uhp+qFKoSJnKAhDcnK0OsdcVH+Pbg+YKWENV7VoAkq+qCEF85dwJPzq1KKKBnnVVVPFqvVCfy5W3sEwi7RUNTb44e9XgDNk3Q==', 'xplay3000@126.com', 'head_sculpture/2ec760ca5a76461299c845cfcd1861bc.jpg', '1', '2021-04-22 14:26:26', '2021-04-22 14:26:26', 'wanjie', 'wanjie');
INSERT INTO `sys_user` VALUES (888889, 'wanjie2', 'wanjie2', 24, '18623234493', 'xplay1', 'pFdygHjwXVA4pG8Cy9HZYedrYVSJ/IjiBvlOb/OEojrJJ0pxjGsR6L4u26xx8bOVF+Xz9f7lwHDcZzhcYZdYfkZXmixhgPb59nvFbMnXEmQvhSNzl/AX805fjFIBTBAfkbIhzF5DMJS80B1TfhN1x3tDJZDIeL4inaUsIzj22gYxkTUCCAYjFYrskpPaEf6kk3fON+QZWr0wsJPjpF1PLJSk73IasfLdSmf713PP/rhcq4G894te5uhp+qFKoSJnKAhDcnK0OsdcVH+Pbg+YKWENV7VoAkq+qCEF85dwJPzq1KKKBnnVVVPFqvVCfy5W3sEwi7RUNTb44e9XgDNk3Q==', 'xplay3000@126.com', 'head_sculpture/ee8d22b512ee48fca3ee81d30b386dcb.jpg', '1', '2021-04-22 14:26:26', '2021-04-22 14:26:26', 'wanjie', 'wanjie');
INSERT INTO `sys_user` VALUES (888890, 'wanjie3', 'wanjie3', 24, '18623234493', 'xplay2', 'pFdygHjwXVA4pG8Cy9HZYedrYVSJ/IjiBvlOb/OEojrJJ0pxjGsR6L4u26xx8bOVF+Xz9f7lwHDcZzhcYZdYfkZXmixhgPb59nvFbMnXEmQvhSNzl/AX805fjFIBTBAfkbIhzF5DMJS80B1TfhN1x3tDJZDIeL4inaUsIzj22gYxkTUCCAYjFYrskpPaEf6kk3fON+QZWr0wsJPjpF1PLJSk73IasfLdSmf713PP/rhcq4G894te5uhp+qFKoSJnKAhDcnK0OsdcVH+Pbg+YKWENV7VoAkq+qCEF85dwJPzq1KKKBnnVVVPFqvVCfy5W3sEwi7RUNTb44e9XgDNk3Q==', 'xplay3000@126.com', 'head_sculpture/c8f4682c48374755b331533bd75faa50.png', '1', '2021-04-22 14:26:26', '2021-04-22 14:26:26', 'wanjie', 'wanjie');
INSERT INTO `sys_user` VALUES (888891, 'wanjie4', 'wanjie4', 24, '18623234493', 'xplay3', 'pFdygHjwXVA4pG8Cy9HZYedrYVSJ/IjiBvlOb/OEojrJJ0pxjGsR6L4u26xx8bOVF+Xz9f7lwHDcZzhcYZdYfkZXmixhgPb59nvFbMnXEmQvhSNzl/AX805fjFIBTBAfkbIhzF5DMJS80B1TfhN1x3tDJZDIeL4inaUsIzj22gYxkTUCCAYjFYrskpPaEf6kk3fON+QZWr0wsJPjpF1PLJSk73IasfLdSmf713PP/rhcq4G894te5uhp+qFKoSJnKAhDcnK0OsdcVH+Pbg+YKWENV7VoAkq+qCEF85dwJPzq1KKKBnnVVVPFqvVCfy5W3sEwi7RUNTb44e9XgDNk3Q==', 'xplay3000@126.com', 'head_sculpture/c3117889674348f9860fd4ee6589cadf.jpg', '1', '2021-04-22 14:26:26', '2021-04-22 14:26:26', 'wanjie', 'wanjie');
INSERT INTO `sys_user` VALUES (888892, NULL, NULL, NULL, NULL, 'wanjie', 'EpCiFNw+mSacjrkPLxuubAxFOv/vcpINY9imDXFC5ahwuZBkTXje9DhhdjEUtv2FqvWz5n74xgci4VTmNrrZXPwtaz7mysbanUeDdTScezgdF/EXrdzkF8sIjIXwFEaxEMN429aSZRmtVJqQZGsTpz3k+EbTAHgwJp5iLnaunM9B+Bn4Fy6B2GAXnqSpZYrpfFW613zDlEaTjpFGUFiw5ZpfVjR3o89zvqcVkM1zmecZolirjcU7rto/aGetJffEDasiXXNCgHlIyT+h8nrjXhpXiuQPlhBZ4EoIrHCN8dvCvyKszsB+VaZKpWXv3uKBFaXQ7TyMLdCJKM7pviwJZw==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888893, NULL, NULL, NULL, NULL, 'wanjie2', 'ni1gA7MimTcWuE2XufcbZLSxoFM4vx5Cq8IIFGnoegxRldulKC9u5P00y71IUHbY0+QH5taD+XdZF7N5autKg1Cc/mtCZ0pHfMp6i4mV6PpllLypUYkN9XF5hPfkdvRSxme7Dv0f1xeX5MpvnmXT3aAD6qnMwnowqkS53AuGrMIlomEbRaR911l946WuDR+sMaFBBYA/7I1hgPnvl4zDks/xgiaMd91jnGkEdBqW053TH+7a66PAmnDbMc+Kf8TAm2N+2ZDCeeFNMavBQEfDhU+Vb+n3tf3AYEKUHy0BIhKpKCHNi6aPq7dHW9uqmqH28eUuKXZMd+qUC49wThxB6A==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888894, NULL, NULL, NULL, NULL, 'wanjie111', 's8Z9E55+r9xe0BSihAweRq73hWH+Pb9HV4oPHWQ32k1YYlznLlZTbA3UdQzrtSmhIkrULj5haOA76/B4BZq36XzeM2ZwtNvQT8X7qLnHU53M2QxRzIYXsucGZTkpZsYcZjQFjM0DjkL7gZ/+hG7avv327AUlSjw1veW6zH04ArOZrw0E7SfJnhJQNnWAj2FkYzJ/B98aVXfPmzttQDpcDQPaQ0/YDib7z8bFUnrPcfIDKltCd+pq5xNpUxLXnbnvMhVnAurq5fBavOkyXqkbsn8TVZmvO6ingRkxPUMTcWzgDse8GO9nujiD1w1p+166BQDMSDe7KAlu62TbwP4aTA==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888895, NULL, NULL, NULL, NULL, 'wanjie22', 'McNnI5C/xWWA3vNR4zTxXyqGtb1VKP1TjzUpaGXuvmR2aVHAdZzswsV1aHK2xsyaBkt2b/tA/oyxFBI2c+TEmv8LYeXcbZ3r6S3pMO+zc0OHB05ZDrXAmAQFYK+8rcZmkHzsDPhuyPLwzqRaAO6EQYaR7UsKU2NTt8Q/rp07yFVZ+Fe6TuSJ+QCbn2kxuyKGH6YsLwvpeJNuRC4K/KuEu+2utP1NgF2bVEunToGRaMdB2ddnIanYASQxxIg7VacWt/quTrwnYsmGFwMEdWowS4wH5nOebx8tFW617qDzC2hD8hceVyi2eEMdULX/qZJmirGzkmngpBgsFx3IfUINCg==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888896, NULL, NULL, NULL, NULL, 'waaaa', 'GzIGzVt9dTZkjUsK0Eufeu8yfPZ5i0MEZ86zV63bTI5Ydu1NLNNKdJ/l83x5UINjmCDRRblxpKORJIO85a+XqY6mzJTHWvud8qTkcJZpTLgf7ap3ug5XBNU97uxkIvdgk8XCm+YQFc9CPRoTfqn1iGKF7zSvbUUV9LiPH9YyBr1E6ZmnEmv49jV9uDS0HezmtFsObzjpovm8BLxIF+Pw6qk3YUZzZZqWVQ+m7APnET5IPAn30E53xBY75cxD9KpqRgVfvK+/j8yU1aj1KKt596Xf0KaxpDiRKG2YPFLmraUgGv3U1pvQmVuxOIpDUqHOXIonAJ+s1BauuduK+1Hkhg==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888897, NULL, NULL, NULL, NULL, 'xxxx', 'EFkDIvKb7W7HJSnxejuTYGfQE0wASdMgj6z1Z//jtwEpNmKL/gXjWcVzhFQWM4XGeknBl3sNdd3vmA4u1XxjmYtztFDic6VcFXOMDWkNVUb4alZadVX7ldQ7IX5PBrlH65QJ8ICf/mLH4V9XcVE8vaDGHzR8AGr7xLMTJTrCvchqbASqWLCVE9PnYDe4igD+Ma35P17MEMPap2BhhqSSu29eLtoTuwUpe3i/50zS5+ddn87uhvgoR2Y1C+33LRQBNfAvFEOST/8dL/xV3oxdB3hqvX+JpytvKgNvYZm6C5Q91OqL3ObKsUdYxamXEqxlluD+iYBUPsCt0KxC17L1GA==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888898, NULL, NULL, NULL, NULL, 'qazw', 'boVAnYMcaZrFCma3KTo0npnbcQg+9Fumln35xZsHBsUnHgxsDUXj6MjTv8fYVe0LOVaw7uq2lXIsEb1I9PJABk30sUAreO6PGAlj4jb9TAPe0FSeanpmZIax0SoNmU0we6SKOi8q15+N2Qz+Ovxxxp/uRAdzleij1ktoEOhFYKRbe7xx0KKjKVVZMmu+un5GR1ak2SUoyueIcFNjaJfIyv1l+c3od+ycSTIyZ1BnbWNgp1amwIDP7/CNpZV2zD29c6U2PiHVZg8aIYQejK4K2y9MVbIkXJYJlOmQkAe0TOdiy+/ebPWoHLSgkpGvl4L2Qxa0QmP9CXKU4OjPJY34sQ==', NULL, 'head_sculpture/93c67e5d3fda4b57b29a578721f3ddea.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888899, NULL, NULL, NULL, NULL, 'tttq', 'AXgSB+Q0mqtoxAVTUjPaTjscaAPVuBAVncmdUfRhoAHSHmG8w/urvtU9+GtNRtSPOg4NEKKLtezcbdsNJRtn0FELNQKrfjUk/Z5pMxzsuztUOb8hWuCzpdOmXoG5WDwPYZwJ7awYE4M43QudmUd0V3eVjX0+ximD51BegXXgEZag0mGuJq736HTZHuL3WrWYPznqqv/NGqXpxoPr2YwNGSt/lCuxy7zGCjCeR/IWeLHtlPOeIAMzhOkkK+hZEdmApfeKyOgFuD2aG2sQLwpUVabyLhdqaUzrtCNrX4MwMkaY34/s82tii2cG3YG3LQKRu24MMv7AJPBd6c/aqxbNJA==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888900, NULL, NULL, NULL, NULL, 'ceshi1', 'POmKT28PKzeneU8Vl4TMwzQS1cA7PLPJQAo2IXRUHTYRV9kVFi5r6oB+MoiA+h/+t4ha3NcAkn/z+t5W4XuZlgIoS7afswW3SG8UOsR9qruRqAidWIqm30nM6jTiitY6adt5SoFHxzIyxI7vXBem215iErc9rfI38jQijYCbSygF/pP1zPwmbaOPgzMcta8eOjKEfXfvN2C1R8S3VdQrS75POs3ipnCOvS2kggmzisSw2auY/osRkNcRCV0Jfu/yym7Ux1g1Nka81X0wvQ+62is1XplOrVb38otrFbD2cBZmkkg188MVNPWK9fotiA3aoL/CrfGI+RkNUBwUq3m7+A==', NULL, 'head_sculpture/969007c646f9480fb5a0265000cabac2.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888901, NULL, NULL, NULL, NULL, 'ceshi2', 'UyJHCN8CKUqcb5Gx1yirTPtXZcDeTdGsDFnUYjoXmCrS1GUbf1DHKMrUdPzrjTaAdIyCT/wLBR4iYAuoxorOh6cQk9/Czuic364qOWjsjBPlUZ5DvDNzZ7yZ0MJV8Qjb4IwO8TM0JcO3iiVJIX3jNtesOGyo7OX7qTsIgIVwQL5V09yJomwH50Ss+sm8K20S+oFEmtYpBdPg7sYSj9mhq1ArUyBAfg/S/xN35XZAzp8qrlmxhfKgyHl2iDKxrVlrALzykQQGWVlJzzPWalo7m5n9hB+NpOUsYOembMG5BK2GkkXfDYfBQEc05LIrxvpXxLZm269STyPXZ2ZJHuQ1dw==', NULL, 'head_sculpture/2d9bbd80436d41b59d4065aac857d992.png', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888902, NULL, NULL, NULL, NULL, 'ceshi3', 'S6FJRZgT+a7vIE5XG53IN5e3aMJEURmjQg5uaH7TjVyJ7ae1vZrDpdlHT9qlK8pwq/lbNYCTfulFSqlW2exrqoqrsUXBROtM37HpIjgt49dwu8UdGP08YY5PR00pzuIdW7/sYX+XPzVcWohmsFjC3LgOvRjsdtD3h4SbVHo/DYQTeG+sUSYUqGRLx+VjCEDWsp79ljpWceouzCPST6Fo/caAaKKRp/TvMSAD2aHk3mGYSrS2tZ/YClfHBmB/S9R2TQklgGUx+ymwE+9JzAEWKqkIiIkwMTfaxrR/ePVNhfKw1z3v9x4mFmrvwtE8RmAnddkIESYZ3gRHOtvQdYpNZw==', NULL, 'head_sculpture/fb1cf363772b415bba31d1f5f58e6c5a.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888903, NULL, NULL, NULL, NULL, '11111', 'SG7vY6L+Lm+TSDQHsH6NqW4+J4Va1YPDprIlf+u+lUJqgoZngLKyx5Ep3T4guM8a8Ba1DBTtXEyUQV/WMRxeXll2iz687j5us0lgMU24OhbTJIdwhwviLoJsJ4T9wWpNcjIpGYM+XCRZjqrfANF+haTbGpqLc3oOZCkCJG9z40JrPBLLola81MvQdqmBrT5CK1fDMEJrIF647V++Ad7tFM2Qcj7b6L4IkaaMCX4EBMogpH5uaVR5UCwDCh3cZm+YfeoslU8G2zfeTkI+SX18005HWbPkBz6/E+j/9j/6lxWyaeo7FRvk6/rAOFTUq3icrUIP+5bRjNB6stbz+KgxLQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888904, NULL, NULL, NULL, NULL, '111111', 'Zz21DgGeQ7g9SXw7U6MNu7eNotMVsUZIOJ6u2NAt86RrvYtLYsOBchMLsNUbhmwCWr2wOurB0cq+IvgaK26sSE1PmGpUs3JCU0Vy/fKrhndgWmTqpyi7sWqIcqQmIGcsVXULn66zWVxytsrKhRJMAbHgROmb4mUWAOwwKvGIrtDsS8Wtl2JRVVmYC3yeGMNDi8J/CqyT/L3q0cGvSQZJpVhrcbYnSkE2QfFdWqFSTAQKGGnPdG6koopAZCU55lF1kAYJrOvmr3WHsLPtRWiM1/IFfwKMOIBSWHiZrmwO0EpUIOQjHlYMYuOwET+zR396qU18OgkSV9YOtcoVFYde6Q==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888905, NULL, NULL, NULL, NULL, 'xieshuang', 'tfUL7+qU/ULJz3/IeJpUmyOfGk5cPBla7udylX0MIvQ15Bq9TE7q9QygrDh4Peic/w/3L0gKIUYZ3hT+zV7yOLomzTVHjD6sG8v+t6il/tfA6Hcuy4ARBeXjj6JgGHOKCg6nuJJKOxtdguhmvmOAAOtOBZyQ8m33SdNk7nf1uV+1CyFyZadUCY/4/ZLgh0Mq8JsltcV24QAlBwANUaO7jhmXGNc1KrB/yMK7w5I4VAMYFR8Hn+G8DtPIT7v7gGTn1VZh1hBElHSrHNklkjWPMwPy7tYtnSx3aUE0BDlV/9quwLoBb+0nvBTufG5gAe5vzxjzIO1DBy6cAo81hD9hDg==', NULL, 'head_sculpture/8ed82b79641044edb91a97149f32ef72.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888906, NULL, NULL, NULL, NULL, 'creates', 'hWeXrvpB8odlwQ7HtA1TXsm7HknYN0UHXfmWLScupU7jfZLCcMeVyeP84KVibleEJIBqJN45BVDIw1xb+atwyaSd5uKvX4lM7Kj+vebfmntE9KYEIq3KXqFCbUg0b7ZQpgbePHNlIo66mi3t2oKa4U9ZesrRu66VH0/evGxnaHhRXzb8B747rsEqTfDCsQAvX0mQlLnQRR65XdEi9OgvsYjEW2t3MErtyoyv3a62kn0V/d5t2V0kR+sx05N186gc7qCUZk++cSTAWxX/XiXKL+DWhoLfXgL6m3UMzA5XBZBQJM1v71iyxSJI/zNgv9GSawGJ+OyCNfMFH8JySZX4MQ==', NULL, 'head_sculpture/2dbd6d7f83594df5883491cfbf680581.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888907, NULL, NULL, NULL, NULL, 'txgtxg', 'BogaXw8JJHH/XnCi/A3OKgiFB+Nlz+dlKQhy1ai+A40YjmuV7TL3h1AGdwERkcWhsEjvC1JGBkTt1t8YP859UYEYbBwByRAeJxGtA3J2RuZJqz3ohLhnG4w6nDA8RaKaVDwZQIz0VwhGnM6NtnIBlme/NjQ72ruKtx9jtXSWzWUsKP6nLk6rdwi/u9I2MSBPgrIiDIja2DIl5mUNMC0aoGiApQAbwQy6SNIUPmYHvzvOcCSgghXjPDIjn//z3DA6zz4a5hpChda4aC/Y8bkKZJ0OZ5+RdTg6WAWRgIeoa6ufSv3r3kbb1iDgtbv6PY+JZVmKM9OIT+TaDxEhgjqGrA==', NULL, 'head_sculpture/f3a613d9f7034b9986e45743b2830993.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888908, NULL, NULL, NULL, NULL, 'guagua11', 'ZuQsP8epY13+JJWrwkFjFm5LJTQvVli5ZunRHrjTmH3m01YZw9AtJTlj1C35pv6I4FgvSqLDOvNYkmp1EKtKBZTa0S8V2OA9tGAlRYy38vNy6tYcSTUgT2jxlJiJXoqbLrOMM12pXkYFED/DQrkhfU0+03krSk2lqNWGBSBLmEl4+Yy5YckeCNwWQ2023CsnbN551UPTbBTSVYjqtywcarxhwqntA1n5Z9hWWZ/rQ3AnrOUYKZbvBoBMNzOcdysNZjLy/Uw0hMckYG0uoIV+jnysNHx+kMsxxbATT2RG2nhLcibDvJ0BjviU9j6/e0bgA3CwoMrMd1Nc3UKf7z3vOQ==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888909, NULL, NULL, NULL, NULL, 'aaaaa', 'm8oHiPg5X1IjSkQlc8IpARJ1OFFF5WwsmzYoXDysJqS/8dRiMaJM+phlcMT4I3P+xgAUY7MgZBsWVrULh2xqQkax2dL7wTbo8vZqI9oDKGxWrkQGX4UQ79jZFVyUfCpH4woljtQZuaE4EyCmKoTSbBQkPRRSZLEGrYC20Wi/fWLFNqAV4bVZIymPwSf5kL42kRBcNCVSSI501uAK+OHGd5Pqy7GXe/EgkAdb1T6hSwUtYtNV9ZDgv4YgDHrIUjtu8Vyn1lr3zudhuqCdFhA/UNWo9/IZ6UOzQCVqUy0q9shHgI38/D9kkAw2yfhELoEEIwMjIPLtytSc0XX1eZ7r/Q==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888910, NULL, NULL, NULL, NULL, 'jiujiu', 'AGXDLp6shq+Gh1goTFs57qaXiOw6GWIrkQuYtvblXo4JrD1p6MWq60KXEwH8r4X0qK/euOnQqQDXLvFZ4dJjkRnv44I8+gEneEnZkshkUOymWbqNP5CCP7Rlh9Ivpgf9loN0bJ27B2uZ6Ec8dYPUgJVJzfMO1jeoRn+nwUk5C29cKYzsn16JbQy64ruzs63O20a+Sx71KB0QT+An6L2fYmwCpfTlseaxv6dR2KVl0r8YPKJl9klwDcdOVPGBr59sqhw3mrXZW08xOJlrfRsMhLQle1sM0VjNfSm3O9IAx5eQmxJabNT7mQ9Jrd1qtomj7WYEXqCeZYvcoWtX/R/SvA==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (888911, NULL, NULL, NULL, NULL, 'zeng', 'Cetv3iINSaTH9Nn4+PTCHGJleprMmqTTFsvT7DmSyt2dmnNDavzpk6O91f+C6GfkV2YCX86aXA6P5KdgB345vC3m6SqTaNm1MUQW/H2O61cwNoMptAvYxkm4b4UCungQUJ0XfkYKRFg8UdvTpP2A1CS8fCaeMehINSTdlrWPDq7LKKGW0SxmKLwWbPI6Q2IkM45omb78ot6+po7YlhoPGidGrjeidkS9roeCfMtjPbowUkz+hlOuyimJwqnnlJRXILXs0pJapWsz8uGzh9K7v+iXn2/kLqH6fQ/K4390Bzx9ix5nASgRUsZX2K0cOrirs98KgbVdXV6Mf/xK7UDbEg==', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for tm_room_log
-- ----------------------------
DROP TABLE IF EXISTS `tm_room_log`;
CREATE TABLE `tm_room_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `room_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_date` datetime(0) NULL DEFAULT NULL,
  `update_date` datetime(0) NULL DEFAULT NULL,
  `room_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `action_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `room_log` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `action_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `action_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tm_room_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
