package com.xplay.xpocker.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author mr.wan
 * @since 2021-05-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CrRoomInfo extends Model<CrRoomInfo> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;

    private String code;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

    private String creater;

    private String updater;

    private String roomCode;

    private String roomType;

    private String roomState;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String createStr;


}
