package com.xplay.xpocker.entity.mahjong;

import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * @author wanjie
 * @date 2021/4/15 10:21
 * <p>
 * 账单信息
 */
@Data
public class MahjongBillEntity implements Serializable {
    private static final long serialVersionUID = 3262476822994975617L;
    private String sourceId;
    private String targetId;
    private String action;
    private String type;
    private Integer fraction;
}
