package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wanjie
 * @date 2021/3/24 10:57
 * <p>
 * 当前房间 用户需要做的事情
 */
@Data
public class MahjongUserTask implements Serializable {
    private static final long serialVersionUID = 1555858426968760083L;
    private String userId;
    private CopyOnWriteArrayList<String> taskArray;
    private String task;
    private boolean parallel;
    private String doTask;
    private Integer cardId;
    /**
     * 当前任务是否已经处理
     */
    private boolean dealWith = false;

    /**
     * 是否可以直接移除
     */
    private boolean removeTag = true;

    public MahjongUserTask() {
    }

    public MahjongUserTask(String userId, CopyOnWriteArrayList<String> taskArray, String task, boolean parallel) {
        this.userId = userId;
        this.taskArray = taskArray;
        this.task = task;
        this.parallel = parallel;
    }

    public MahjongUserTask(String task) {
        this.task = task;
    }

    public MahjongUserTask(String userId, String... tasks) {
        this.userId = userId;
        if (tasks != null && tasks.length == 1) {
            this.task = tasks[0];
        } else if (tasks != null && tasks.length > 1) {
            taskArray = new CopyOnWriteArrayList<String>(Arrays.asList(tasks));
        }
    }

    public MahjongUserTask(String userId, String task, boolean dealWith) {
        this.userId = userId;
        this.task = task;
        this.dealWith = dealWith;
    }


    public MahjongUserTask(String userId, CopyOnWriteArrayList<String> taskArray, boolean parallel) {
        this.userId = userId;
        this.taskArray = taskArray;
        this.parallel = parallel;
    }
}
