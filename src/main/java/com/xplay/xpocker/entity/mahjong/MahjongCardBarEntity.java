package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/4/16 10:25
 */
@Data
public class MahjongCardBarEntity {
    Enum cardEnum;
    Integer cardId;

    public MahjongCardBarEntity(Enum cardEnum, Integer cardId) {
        this.cardEnum = cardEnum;
        this.cardId = cardId;
    }

    public MahjongCardBarEntity() {
    }
}
