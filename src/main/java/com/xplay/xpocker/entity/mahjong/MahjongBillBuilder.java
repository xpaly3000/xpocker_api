package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wanjie
 * @date 2021/4/15 10:21
 * <p>
 * 账单信息
 */
@Data
public class MahjongBillBuilder implements Serializable {
    private static final long serialVersionUID = 3884927341727509003L;
    private MahjongBillEntity billEntity;

    public MahjongBillBuilder() {
        this.billEntity = new MahjongBillEntity();
    }

    public MahjongBillBuilder init(String action, String type, String sourceId, String targetId, Integer fraction) {
        this.billEntity.setAction(action);
        this.billEntity.setType(type);
        this.billEntity.setSourceId(sourceId);
        this.billEntity.setTargetId(targetId);
        this.billEntity.setFraction(fraction);
        return this;
    }


    public MahjongBillEntity build() {
        return this.billEntity;
    }
}
