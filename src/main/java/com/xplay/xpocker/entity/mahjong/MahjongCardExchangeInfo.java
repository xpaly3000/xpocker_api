package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wanjie
 * @date 2021/3/25 18:28
 */
@Data
public class MahjongCardExchangeInfo {
    // 当前用户
    private Integer current;
    // 拿谁的牌
    private Integer inCard;
    // 你的牌拿出去给谁
    private Integer outCard;

    public MahjongCardExchangeInfo(Integer current, Integer inCard, Integer outCard) {
        this.current = current;
        this.inCard = inCard;
        this.outCard = outCard;
    }
}

