package com.xplay.xpocker.entity.mahjong;

import com.xplay.xpocker.meta.user.MetaUserChannel;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wanjie
 * @date 2021/4/14 16:01
 */

@Data
public class MahjongBillClearing implements Serializable {
    private static final long serialVersionUID = -5539966004665616241L;
    MetaUserChannel userInfo;
    Integer userWinSeq;
    boolean winState;
    int fraction;
    List<String> fractionResources;
    CopyOnWriteArrayList<Integer> userCards;
    Integer huCard;
    ArrayList<MahjongCardTouchBar> cardTouchBars;

    MahjongBillClearing() {
    }

    public MahjongBillClearing(MetaUserChannel userInfo, boolean isWin) {
        this.userInfo = userInfo;
        this.fractionResources = new ArrayList<>();
        this.winState = isWin;
    }
}
