package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

import java.util.ArrayList;

/**
 * @author wanjie
 * @date 2021/4/1 11:40
 * 用于记录碰还是胡 还是杠  还是明杠  暗杠  点杠
 */
@Data
public class MahjongCardTouchBar {
    String userId;
    CardEnum cardEnum;
    ArrayList<Integer> cards;
    public MahjongCardTouchBar(){

    }

    public MahjongCardTouchBar(String userId, CardEnum cardEnum, ArrayList<Integer> cards) {
        this.userId = userId;
        this.cardEnum = cardEnum;
        this.cards = cards;
    }
}
