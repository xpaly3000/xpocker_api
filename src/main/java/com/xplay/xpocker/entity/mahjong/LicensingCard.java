package com.xplay.xpocker.entity.mahjong;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author wanjie
 * @date 2021/6/9 9:53
 */

@Data
@AllArgsConstructor
public class LicensingCard {
    private Integer licensingCard;
    private MahjongUserTask userTask;
    public LicensingCard(){

    }
}
