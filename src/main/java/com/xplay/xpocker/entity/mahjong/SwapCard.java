package com.xplay.xpocker.entity.mahjong;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wanjie
 * @date 2021/5/30 10:57
 */

@Data
@AllArgsConstructor
public class SwapCard implements Serializable {
    private String userId;
    private CopyOnWriteArrayList<Integer> cards;
    public SwapCard(){

    }
}
