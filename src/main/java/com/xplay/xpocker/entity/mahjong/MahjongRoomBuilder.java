package com.xplay.xpocker.entity.mahjong;

import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.room.MetaRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.JacksonStringUtil;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.io.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author wanjie
 * @date 2021/3/17 20:02
 */

@Data
public class MahjongRoomBuilder implements Serializable {


    private static Logger logger = LoggerFactory.getLogger(MahjongRoomBuilder.class);


    private MahjongRoomEntity mahjongRoomEntity;

    public MahjongRoomBuilder() {
        mahjongRoomEntity = new MahjongRoomEntity();
    }

    public static MetaRoomEntity inGame(MahjongRoomEntity roomInfo, MahjongUserChannel currentUser) {
        MahjongRoomBuilder roomBuilder = new MahjongRoomBuilder();
        MahjongRoomEntity roomBuilderEntity = roomBuilder.getMahjongRoomEntity();
        if (roomInfo.getCurrentExportUserId() != null && currentUser.getUserId().equals(roomInfo.getCurrentExportUserId())) {
            roomBuilderEntity.setLicensingCard(roomInfo.getLicensingCard());
        }
        roomBuilderEntity.setCode(roomInfo.getCode());
        roomBuilderEntity.setExchangeValue(roomInfo.getExchangeValue());
        roomBuilderEntity.setGameEndDate(roomInfo.getGameEndDate());
        roomBuilderEntity.setCurrentExportUserId(roomInfo.getCurrentExportUserId());
        roomBuilderEntity.setRoomType(roomInfo.getRoomType());
        if (roomInfo.getState() == DictionaryConst.MahjongRoomState.BILL_CLEARING || roomInfo.getState() == DictionaryConst.MahjongRoomState.OVER) {
            roomBuilderEntity.setBillClearingInfo(roomInfo.getBillClearingInfo());
        }
        roomBuilderEntity.setRule(roomInfo.getRule());
        roomBuilderEntity.setGamesCurrentNumber(roomInfo.getGamesCurrentNumber());
        roomBuilderEntity.setState(roomInfo.getState());
        // 过敏信息处理
        HashMap<String, MahjongUserChannel> allUserInfo = roomInfo.getAllUserInfo();
        HashMap<String, MahjongUserChannel> allUserInfoClone = new HashMap<>();
        // 借助Spring 的深度拷贝   避免 对象引用 对原始数据造成修改
        BeanUtils.copyProperties(allUserInfo, allUserInfoClone);
        allUserInfoClone.forEach((uid, userInfo) -> {
            // 胡牌信息
            userInfo.setCallHuCards(null);
            // 交换信息
            userInfo.setUserSwapCard(null);
        });
        roomBuilderEntity.setAllUserInfo(allUserInfo);
        roomBuilderEntity.setCurrentUserInfo(currentUser);
        MetaRoomEntity result = roomBuilder.setUserExportCards(roomInfo.getUserExportCards()).setOtherInfo(roomInfo, currentUser.getUserId())
                .setCurrentUserTask(roomInfo.getUserTask(currentUser.getUserId()))
                .setCurrentUserCards(roomInfo.getCurrentUserCard(currentUser.getUserId())).setAllCardCount(roomInfo.allCardLength()).builder();
        logger.info(JacksonStringUtil.objectToString(result, false));
        return result;
    }

    public MahjongRoomBuilder setOtherInfo(MahjongRoomEntity roomInfo, String currentId) {
        HashMap<String, MahjongUserChannel> allUserInfo = roomInfo.getAllUserInfo();
        HashMap<String, MahjongOtherInfo> otherUserInfo = roomInfo.getOtherUserInfo();
        if (otherUserInfo == null) {
            otherUserInfo = new HashMap<String, MahjongOtherInfo>();
        }
        for (Map.Entry<String, MahjongUserChannel> user : allUserInfo.entrySet()) {
            if (!user.getKey().equals(currentId)) {
                MahjongOtherInfo otherInfo = new MahjongOtherInfo();
                otherInfo.setUserCardLength(roomInfo.getCurrentUserCard(user.getKey()).size());
                HashMap<String, ArrayList<MahjongCardTouchBar>> userTouchBar = roomInfo.getUserTouchBar();
                if (userTouchBar != null) {
                    otherInfo.setTouchBars(roomInfo.getUserTouchBar().get(user.getKey()));
                }
                otherUserInfo.put(user.getKey(), otherInfo);
            }
        }
        mahjongRoomEntity.setUserTouchBar(roomInfo.getUserTouchBar());
        mahjongRoomEntity.setOtherUserInfo(otherUserInfo);
        return this;

    }

    public MahjongRoomBuilder setUserCards(HashMap<String, CopyOnWriteArrayList<Integer>> userCards) {
        mahjongRoomEntity.setUserCards(userCards);
        return this;
    }


    public MahjongRoomBuilder setCurrentUserTouchBarCards(CopyOnWriteArrayList<Integer> userTouchBarCards) {
        mahjongRoomEntity.setCurrentUserTouchBarCards(userTouchBarCards);
        return this;
    }

    public MahjongRoomBuilder setCurrentUserTask(MahjongUserTask currentUserTask) {
        mahjongRoomEntity.setCurrentUserTask(currentUserTask);
        return this;
    }

    public MahjongRoomBuilder setCurrentUserCards(CopyOnWriteArrayList<Integer> currentUserCards) {
        if (currentUserCards == null) {
            return this;
        }
        currentUserCards.sort(Comparator.naturalOrder());
        mahjongRoomEntity.setCurrentUserCards(currentUserCards);
        return this;
    }

    public MahjongRoomBuilder setAllCardCount(Integer size) {
        mahjongRoomEntity.setAllCardCount(size);
        return this;
    }

    public MahjongRoomBuilder setUserExportCards(HashMap<String, CopyOnWriteArrayList<Integer>> userExportCards) {
        mahjongRoomEntity.setUserExportCards(userExportCards);
        return this;
    }

    public MahjongRoomEntity builder() {
        return this.mahjongRoomEntity;
    }
}
