package com.xplay.xpocker.entity.mahjong;

/**
 * @author wanjie
 * @date 2021/3/29 11:18
 */

public enum CardEnum {
    // 暗杠  明杠  点杠
    TOUCH,ANGANG, MINGGANG, DIANGANG;
}
