package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/5/30 10:58
 */
@Data
public class UserSwapCard {
    // 拿的谁的牌
    private SwapCard inSwapCard;
    // 拿出去给了谁
    private SwapCard outSwapCard;
}
