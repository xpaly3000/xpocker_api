package com.xplay.xpocker.entity.mahjong;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author wanjie
 * @date 2021/4/1 11:55
 */
@Data
public class MahjongOtherInfo implements Serializable {
    private static final long serialVersionUID = -6928847714518711553L;
    private int userCardLength;
    private ArrayList<MahjongCardTouchBar> touchBars;
    private Integer huCardId;
}
