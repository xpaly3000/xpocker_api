package com.xplay.xpocker.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.xplay.xpocker.entity.system.UserVO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author wanjie
 * @date 2021/6/3 23:31
 */
@Data
public class RoomInfoVO {
    private Integer roomId;
    private String roomType;
    private String roomCode;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime startDate;
    @JsonFormat(pattern = "HH:mm", timezone = "GMT+8")
    private LocalDateTime endDate;
    private List<UserVO> users;
}
