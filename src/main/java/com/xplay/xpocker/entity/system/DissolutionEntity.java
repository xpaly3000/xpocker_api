package com.xplay.xpocker.entity.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;

/**
 * @author wanjie
 * @date 2021/6/1 20:05
 */
@Data
public class DissolutionEntity {
    /**
     * 解散开始时间
     */
    // 3分钟后 解散指令失效
    public static Integer invalid = 3;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dissolutionStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dissolutionRemainingDate;

    private long remainingTime;
    // 前段需要头像和名称
    private HashMap<String, UserDisInfo> userDissolution;

    public DissolutionEntity() {
        this.userDissolution = new HashMap<String, UserDisInfo>();
    }

    public void setDisInfo(String userId, String headImage, Integer agreeState) {
        UserDisInfo userDisInfo = this.userDissolution.get(userId);
        if (null == userDisInfo) {
            userDisInfo = new UserDisInfo(userId, headImage, agreeState);
        }
        userDisInfo.setAgreeState(agreeState);
        this.userDissolution.put(userId, userDisInfo);
    }

    public boolean invalidDate() {
        return getDissolutionRemainingDate().getTime() > new Date().getTime();
    }

    public long getRemainingTime() {
        return (getDissolutionRemainingDate().getTime() - new Date().getTime()) / 1000;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public Date getDissolutionRemainingDate() {
        return new Date(this.getDissolutionStartDate().getTime() + 1000 * 60 * invalid);
    }

    public void setDissolutionRemainingDate(Date dissolutionRemainingDate) {
        this.dissolutionRemainingDate = dissolutionRemainingDate;
    }
}
