package com.xplay.xpocker.entity.system;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/6/3 23:32
 */
@Data
public class UserVO {
    private String username;
    private String portraitUrl;
    private Integer fraction;
}
