package com.xplay.xpocker.entity.system;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;

/**
 * @author wanjie
 * @date 2021/6/1 20:05
 */
@Data
@AllArgsConstructor
public class UserDisInfo {
    private String username;
    private String portraitUrl;
    private int agreeState;
    public UserDisInfo(){}

}
