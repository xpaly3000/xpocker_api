package com.xplay.xpocker.entity.system;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wanjie
 * @date 2021/4/20 20:13
 */
@Data
public class VerificationMode implements Serializable {
    private String backGroundImage;
    private String moveImage;
    private double scale;
    private String id;
    private Double x;
    private Double y;
}
