package com.xplay.xpocker.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Data
public class SysUserDTO implements Serializable {
    private SysUser sysUser;
    private VerificationMode verificationMode;
    private List<GrantedAuthority> grantedAuthorities;
    private List<String> authoritiesList;
    private String tokenAuthority;
}
