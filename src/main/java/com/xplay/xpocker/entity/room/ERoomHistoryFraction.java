package com.xplay.xpocker.entity.room;

import java.io.Serializable;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ERoomHistoryFraction extends Model<ERoomHistoryFraction> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer historyId;

    private String userInfo;

    private String userCard;

    private String fractionInfo;

    private Integer userId;

    private int fraction;


}
