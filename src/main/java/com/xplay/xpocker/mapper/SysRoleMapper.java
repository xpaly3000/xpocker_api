package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.system.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
