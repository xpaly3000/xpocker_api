package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.TMRoomLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-25
 */
public interface TMRoomLogMapper extends BaseMapper<TMRoomLog> {

}
