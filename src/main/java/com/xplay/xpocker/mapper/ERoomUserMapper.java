package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.room.ERoomUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
public interface ERoomUserMapper extends BaseMapper<ERoomUser> {

}
