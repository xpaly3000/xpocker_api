package com.xplay.xpocker.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xplay.xpocker.entity.CrRoomInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xplay.xpocker.entity.RoomInfoVO;
import org.apache.ibatis.annotations.Param;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-05-26
 */
public interface CrRoomInfoMapper extends BaseMapper<CrRoomInfo> {
    IPage<RoomInfoVO> queryUserHistoryRoom(Page<RoomInfoVO> pageInfo,@Param(value = "userId") Integer userId);
}
