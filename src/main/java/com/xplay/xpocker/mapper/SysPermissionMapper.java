package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.system.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    @Select(value = "select distinct sp.* from sys_user su,sys_user_role ur,sys_role_permission rp,sys_permission sp\n" +
            "where su.id=#{uId} and su.id = ur.user_id and ur.role_id = rp.role_id and sp.id = rp.permission_id ")
    List<SysPermission> queryUserPermission(@Param(value = "uId")int uId);

}
