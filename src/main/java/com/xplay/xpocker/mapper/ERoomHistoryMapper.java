package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.room.ERoomHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
public interface ERoomHistoryMapper extends BaseMapper<ERoomHistory> {

}
