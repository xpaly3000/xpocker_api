package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.system.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select(value = "select * from sys_user where username = #{name}")
    SysUser selectByName(@Param("name") String name);

}
