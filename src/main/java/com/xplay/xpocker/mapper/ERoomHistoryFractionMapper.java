package com.xplay.xpocker.mapper;

import com.xplay.xpocker.entity.room.ERoomHistoryFraction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
public interface ERoomHistoryFractionMapper extends BaseMapper<ERoomHistoryFraction> {

    @Select(value = "SELECT sum(ehf.fraction) FROM e_room_history_fraction ehf, e_room_history eh  WHERE user_id = #{userId}  AND ehf.history_id = eh.id  AND eh.room_id =#{roomId}")
    Integer getUserFractionByRoomId(@Param(value = "userId") Integer userId,@Param(value = "roomId")  Integer roomId);

}
