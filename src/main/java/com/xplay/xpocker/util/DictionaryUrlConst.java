package com.xplay.xpocker.util;

/**
 * @author wanjie
 * @date 2021/3/17 20:43
 */

public abstract class DictionaryUrlConst {

    public abstract static class ControllerHeader {
        public static final String sys = "/system/";
    }

    public abstract static class UserController {
        public static final String login = "login";
        public static final String create = "create";
        public static final String update = "update";
    }
}
