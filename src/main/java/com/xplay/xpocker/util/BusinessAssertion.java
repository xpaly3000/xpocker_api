package com.xplay.xpocker.util;

import com.xplay.xpocker.business.BusinessEnum;
import com.xplay.xpocker.business.BusinessException;

import java.util.List;
import java.util.Map;

/**
 * @Author: wanjie
 * @Date: 2019/5/30 16:08
 * @Version 1.0
 */
public class BusinessAssertion extends Assertion {

    /**
     * 用于异常抛出
     *
     * @param objects
     */
    public static void isNull(String message, Object... objects) {
        isNull(new BusinessException(message), objects);
    }

    public static void isNull(BusinessEnum message, Object... objects) {
        isNull(new BusinessException(message), objects);
    }

    /**
     * 判断是否为 true
     *
     * @param message
     * @param parames
     */
    public static void isTrue(String message, Boolean... parames) {
        isTrue(new BusinessException(message), parames);
    }

    public static void isTrue(BusinessEnum message, Boolean... parames) {
        isTrue(new BusinessException(message), parames);
    }
}
