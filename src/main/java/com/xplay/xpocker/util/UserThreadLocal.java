package com.xplay.xpocker.util;

import com.xplay.xpocker.entity.system.SysUser;

/**
 * @author wanjie
 * @date 2021/3/22 15:18
 */

public class UserThreadLocal {
    private static ThreadLocal<SysUser> threadUser = new ThreadLocal<>();

    public static void setUser(SysUser user) {
        threadUser.set(user);
    }

    public static SysUser getUser() {
        return threadUser.get();
    }

    public static void removeUser() {
        threadUser.remove();
    }
}
