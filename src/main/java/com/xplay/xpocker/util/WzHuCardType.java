package com.xplay.xpocker.util;

import lombok.Data;

import java.util.*;

/**
 * @author wanjie
 * @date 2021/3/10 20:31
 */

@Data
public class WzHuCardType {
    String type;
    // 翻数
    Integer turns;
    boolean threeSteps;
    //分数
    Integer fraction;
    String huType;

    public WzHuCardType() {
    }

    public WzHuCardType(String type, Integer turns) {
        this.type = type;
        this.turns = turns;
    }

    public WzHuCardType(String type, Integer turns, boolean threeSteps) {
        this.type = type;
        this.turns = turns;
        this.threeSteps = threeSteps;
    }

    public void addTurns(Boolean... turns) {
        if (turns != null) {
            for (Boolean turn : turns) {
                if (turn) {
                    this.turns += 1;
                }
            }
        }
    }

    /**
     * 封顶计分
     *
     * @param maxTurns
     * @return
     */
    public Integer getFraction(Integer maxTurns) {
        if (this.turns <= 0) {
            return 2;
        } else {
            int result = 12;
            if (maxTurns != null) {
                this.turns = this.turns > maxTurns ? maxTurns : this.turns;
            }
            int turnsTemp = this.turns-1;
            while (turnsTemp > 0) {
                result = result * 2;
                turnsTemp--;
            }
            return result;
        }
    }

    /**
     * 不封顶 计分
     *
     * @return
     */

    public Integer getFraction() {
        return getFraction(null);
    }
}
