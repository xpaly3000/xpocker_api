package com.xplay.xpocker.util;

/**
 * @author wanjie
 * @date 2021/5/19 15:55
 */

public class RegExpConstant {
    public static final String userName = "^[a-zA-Z0-9_-]{4,16}$";
    public static final String password = "^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)])+$).{6,20}$";
}
