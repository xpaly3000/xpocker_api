package com.xplay.xpocker.util;

import java.util.List;
import java.util.Map;

/**
 * @Author: wanjie
 * @Date: 2019/3/1 9:53
 * @Version 1.0
 */

public class Decide {
    /**
     * 判断不为空
     *
     * @param objects
     * @return
     */
    public static Boolean isEmpty(Object... objects) {
        if (null == objects) {
            return true;
        }
        for (Object obj : objects) {
            if (null == obj) {
                return true;
            }
            if (obj instanceof String) {
                String val = obj.toString();
                if (("").equals(val)) {
                    return true;
                }
            }
            if (obj instanceof List) {
                List val = (List) obj;
                if (val.size() <= 0) {
                    return true;
                }
            }
            if (obj instanceof Map) {
                Map val = (Map) obj;
                if (val.size() <= 0) {
                    return true;
                }
            }
        }
        return false;
    }


}
