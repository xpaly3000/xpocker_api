package com.xplay.xpocker.util;

import com.xplay.xpocker.business.BusinessException;

import java.util.List;
import java.util.Map;

/**
 * @Author: wanjie
 * @Date: 2019/5/30 16:08
 * @Version 1.0
 */
//  extends Assert
public class Assertion {
    /**
     * 用于异常抛出
     *
     * @param objects
     */
    public static void isNull(RuntimeException exception, Object... objects) {
        if (null == objects || objects.length < 0) {
            throw exception;
        }
        for (int i = 0, j = objects.length; i < j; i++) {
            Object obj = objects[i];
            if (null == objects[i]) {
                throw exception;
            }
            if (obj instanceof String) {
                String val = objects[i].toString();
                if (("").equals(val)) {
                    throw exception;
                }
            }
            if (obj instanceof List) {
                List val = (List) obj;
                if (val.size() <= 0) {
                    throw exception;
                }
            }
            if (obj instanceof Map) {
                Map val = (Map) obj;
                if (val.size() <= 0) {
                    throw exception;
                }
            }
        }
    }


    /**
     * 用于异常抛出
     *
     * @param objects
     */
    public static void isNull(String message, Object... objects) {
        isNull(new BusinessException(message), objects);
    }

    /**
     * 判断是否为 true
     *
     * @param exception
     * @param parames
     */
    public static void isTrue(RuntimeException exception, Boolean... parames) {
        if (null == parames && parames.length < 0) {
            throw exception;
        } else {
            for (Boolean parame : parames) {
                if (parame) {
                    throw exception;
                }
            }
        }

    }

    /**
     * 判断是否为 true
     *
     *
     * @param message
     * @param parames
     */
    public static void isTrue(String message, Boolean... parames) {
        isTrue(new BusinessException(message), parames);
    }
}
