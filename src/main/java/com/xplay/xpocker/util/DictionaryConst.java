package com.xplay.xpocker.util;

/**
 * @author wanjie
 * @date 2021/3/17 20:43
 */

public abstract class DictionaryConst {


    public abstract static class RoomDissolutionConst {
        //  已同意
        public static final Integer AGREE = 1;
        // 已拒绝
        public static final Integer REFUSE = 2;
        // 待处理
        public static final Integer DEFAULT = 0;
    }


    public abstract static class RoomTypeConst {
        //  房间类型
        public static final String WZ_MAHJONG = "WZ_MAHJONG";
    }

    public abstract static class RedisExpireData {
        /*
        房间失效时间
         */
        public static final long ROOM_EXPIRE = 1800;

    }


    public abstract static class RedisHeader {
        // 麻将房间Header
        public static final String ROOM_HEADER = "ROOM_HEADER:";
        public static final String USER_ROOM_HEADER = "USER_IN:";

        public static final String USER_ACTION_HEADER = "ROOM_HEADER:ACTION:";
    }


    public abstract static class UserRedisHeader {
        public static final String VERIFICATION_CODE = "VERIFICATION_CODE:";
    }

    public abstract static class UserActionConst {
        // 麻将房间Header
        public static final String OFF_LINE = "OFF_LINE";

        public static final String ON_LINE = "ON_LINE";
    }

    public abstract static class MahjongRuleConst {
        // 报叫  SING_UP      SHOPING_CARD
        public static final String SING_UP = "SING_UP";
        public static final String SHOPING_CARD = "SHOPING_CARD";
    }

    public abstract static class MahjongAction {
        // 麻将的行为
        public static final String ADD_HOME = "ADD_HOME";
        // 用户准备 prepare
        public static final String USER_PREPARE = "USER_PREPARE";

        // 消息 包含动画
        public static final String INTERACTIVE = "INTERACTIVE";


        public static final String LOG_OUT = "LOG_OUT";

        public static final String DISSOLUTION = "DISSOLUTION";

        public static final String READ_HOME = "READ_HOME";
        // 摇骰子
        public static final String DISK_DICE = "DISK_DICE";

        // 游戏结束
        public static final String CURRENT_GAME_OVER = "CURRENT_GAME_OVER";

        // 收牌
        public static final String OBTAIN_CARD = "OBTAIN_CARD";
        // 交换牌指令
        public static final String EXCHANGE_CARD = "EXCHANGE_CARD";
        // 出牌指令
        public static final String EXPORT_CARD = "EXPORT_CARD";
        // 摸   碰   杠   胡  过
        // 报叫
        public static final String CARD_CALL = "CARD_CALL";
        // 碰牌

        public static final String CARD_TOUCH = "CARD_TOUCH";
        // 杠牌
        public static final String CARD_BAR = "CARD_BAR";

        // 发牌
        public static final String CARD_LICENSING = "CARD_LICENSING";

        // 胡牌
        public static final String CARD_HU = "CARD_HU";

        // 过
        public static final String ACTION_PASS = "ACTION_PASS";

        // message Tips
        public static final String ACTION_TIPS = "ACTION_TIPS";

        // userupdate tips
        public static final String USER_UPDATE = "USER_UPDATE";


    }

    public abstract static class MahjongRoomState {

        // 创建
        public static final Integer CREATE = 0;
        // 开始了
        public static final Integer START = 1;
        // 结束
        public static final Integer OVER = 2;
        // 账单清算状态
        public static final Integer BILL_CLEARING = 3;
        // 账单清算状态
        public static final Integer CHANGE = 4;
    }

    /**
     * 玩家数字
     */
    public abstract static class MahjongRoomUserIndexConst {
        public static final Integer ONE = 1;
        public static final Integer TWO = 2;
        public static final Integer THREE = 3;
        public static final Integer FOUR = 4;
    }

    /**
     * 塞子数字
     */
    public abstract static class MahjongDiceConst {
        public static final Integer ONE = 1;
        public static final Integer TWO = 2;
        public static final Integer THREE = 3;
        public static final Integer FOUR = 4;
        public static final Integer FIVE = 5;
        public static final Integer SIX = 6;
    }

    /**
     * 玩家数字
     */
    public abstract static class MahjongConstant {
        // 庄家棋牌数量
        public static final Integer BANKER_COUNT = 14;
        // 闲家棋牌数量
        public static final Integer LEISURE_COUNT = 13;
    }
}
