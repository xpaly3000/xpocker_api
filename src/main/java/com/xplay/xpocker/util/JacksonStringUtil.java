package com.xplay.xpocker.util;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.xplay.xpocker.meta.MessageContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

/**
 * @author wanjie
 * @date 2021/4/6 16:37
 */

public class JacksonStringUtil {
    private static Logger logger = LoggerFactory.getLogger(JacksonStringUtil.class);

    public static <T> String objectToString(T t, boolean needType) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            if (needType) {
                mapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
                        ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
            }
            String result = mapper.writeValueAsString(t);
            logger.info("jackson json 格式化===={}===", result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T objectCase( String json,Class<T> clazz) {
        try {
            T result = null;
            result = new ObjectMapper().readValue(json, clazz);
            logger.info("jackson json 格式化===={}===", result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }


}
