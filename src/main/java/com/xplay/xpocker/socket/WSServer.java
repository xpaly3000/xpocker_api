package com.xplay.xpocker.socket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: wanjie
 * @Date: 2019/5/17 14:54
 * @Version 1.0
 */

@Component
public class WSServer {
    @Autowired
    private WSServerInitialzer wsServerInitialzer;
    private EventLoopGroup mainGroup;
    private EventLoopGroup subGroup;
    private ServerBootstrap serverBootstrap;

    //开启服务器
    public void start() {
        mainGroup = new NioEventLoopGroup();
        subGroup = new NioEventLoopGroup();
        serverBootstrap = new ServerBootstrap();
        /*serverBootstrap.group(mainGroup,subGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new WSServerInitialzer());*/
        serverBootstrap.group(mainGroup, subGroup);
        serverBootstrap.channel(NioServerSocketChannel.class);
        // 5.0.0.Alpha1 报错  channel(NioServerSocketChannel.class)
        //连接数
        /* serverBootstrap.option(ChannelOption.SO_BACKLOG, 1024);
        //不延迟，消息立即发送
        serverBootstrap.option(ChannelOption.TCP_NODELAY, true);
        //长连接
        serverBootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);*/
        serverBootstrap.childHandler(wsServerInitialzer);
        //serverBootstrap.bind(8989);
        serverBootstrap.bind(9999);//.sync();
        //this.channelFuture.channel().closeFuture().sync();
    }


}
