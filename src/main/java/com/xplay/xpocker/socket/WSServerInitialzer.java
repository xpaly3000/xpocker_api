package com.xplay.xpocker.socket;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Author: wanjie
 * @Date: 2019/5/17 14:55
 * @Version 1.0
 */
@Component
public class WSServerInitialzer extends ChannelInitializer<SocketChannel> {
    @Autowired
    private WebSocketServerHandler webSocketServerHandler;

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
      /*  ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new StringEncoder(Charset.forName("GBK")));
        pipeline.addLast(new ByteArrayEncoder());
//                        pipeline.addLast("http-codec", new HttpServerCodec());
//                        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));
//                        pipeline.addLast("http-chunked", new ChunkedWriteHandler());
        pipeline.addLast("handler", new WebServerHandler()); // 客户端触发操作*/
        pipeline.addLast("http-codec", new HttpServerCodec());//设置解码器
        pipeline.addLast("aggregator", new HttpObjectAggregator(65536));//聚合器，使用websocket会用到
        pipeline.addLast("http-chunked", new ChunkedWriteHandler());//用于大数据的分区传输
        //  用于心跳 避免某些进程长时间占用连接
        pipeline.addLast(new IdleStateHandler(10, 10, 10, TimeUnit.SECONDS));
        // pipeline.addLast(new HeartBeatHandler());
        pipeline.addLast("handler", webSocketServerHandler);//自定义的业务handler


        // pipeline.addLast(new WebSocketServerProtocolHandler("/ws", null, true, 65536 * 10));
        // pipeline.addLast(new HttpRequestHandler("/ws"));


        // 增加心跳事件支持
        // 第一个参数:  读空闲4秒
        // 第二个参数： 写空闲8秒
        // 第三个参数： 读写空闲12秒
        // pipeline.addLast(new IdleStateHandler(4,8,12));
        // pipeline.addLast(new HearBeatHandler());

        //编解码器

        /**
         * last  不要乱加 加了容易出事
         */
        //pipeline.addLast(new HttpServerCodec());
        // http 编解码
        /*pipeline.addLast(new HttpServerCodec());
        //对大数据流处理
        //pipeline.addLast(new ChunkedWriteHandler());
        // 对httpmessage进行聚合，聚合成fullhttprequest或fullhttpresponse
        // 几乎在netty中的编程都会使用到此hanler
        pipeline.addLast(new HttpObjectAggregator(1024 * 64));
        //字符串解码
        *//*pipeline.addLast(new StringDecoder(CharsetUtil.UTF_8));
        //字符串编码
        pipeline.addLast(new StringEncoder(CharsetUtil.UTF_8));*//*
        //===================以上是用于支持http协议===================
        //websocket服务器处理的协议，用于指定给客户端连接访问的路由
        //pipeline.addLast(new HttpRequestHandler("/ws"));
        //pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));
        //自定义拦截器
        //pipeline.addLast(new ChatHandler());
        // netty 代理 握手，处理 close ping pong


        pipeline.addLast(new HttpRequestHandler("/ws"));
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));

        pipeline.addLast(new ChatHandler());



        // 读空闲9秒激发
        channel.pipeline().addLast(new IdleStateHandler(9, 0, 0, TimeUnit.SECONDS));
*/
    }

}
