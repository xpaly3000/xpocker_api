package com.xplay.xpocker.service.system;

import com.xplay.xpocker.entity.system.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xplay.xpocker.entity.system.SysUserDTO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
public interface ISysUserService extends IService<SysUser> {

    SysUserDTO queryUserInfoByUserName(String userName);

    void addUser(SysUser sysUser);

    void updateUser(SysUser sysUser);

}
