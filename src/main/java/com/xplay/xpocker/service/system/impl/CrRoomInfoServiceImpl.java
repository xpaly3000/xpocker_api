package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.CrRoomInfo;
import com.xplay.xpocker.mapper.CrRoomInfoMapper;
import com.xplay.xpocker.service.system.ICrRoomInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-05-26
 */
@Service
public class CrRoomInfoServiceImpl extends ServiceImpl<CrRoomInfoMapper, CrRoomInfo> implements ICrRoomInfoService {
    @Override
    @Transactional
    public void addCreateRoomInfo(CrRoomInfo roomInfo) {
        this.saveOrUpdate(roomInfo);
    }
}
