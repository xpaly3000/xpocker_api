package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.system.SysRole;
import com.xplay.xpocker.mapper.SysRoleMapper;
import com.xplay.xpocker.service.system.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
