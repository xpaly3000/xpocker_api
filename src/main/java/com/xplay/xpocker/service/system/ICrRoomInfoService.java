package com.xplay.xpocker.service.system;

import com.xplay.xpocker.entity.CrRoomInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mr.wan
 * @since 2021-05-26
 */
public interface ICrRoomInfoService extends IService<CrRoomInfo> {

    public void addCreateRoomInfo(CrRoomInfo roomInfo);

}
