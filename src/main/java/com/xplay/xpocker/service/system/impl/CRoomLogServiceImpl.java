package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.TMRoomLog;
import com.xplay.xpocker.mapper.TMRoomLogMapper;
import com.xplay.xpocker.service.mahjong.ICRoomLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-25
 */
@Service
public class CRoomLogServiceImpl extends ServiceImpl<TMRoomLogMapper, TMRoomLog> implements ICRoomLogService {

}
