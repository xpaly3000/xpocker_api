package com.xplay.xpocker.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xplay.xpocker.business.BusinessException;
import com.xplay.xpocker.config.security.SecurityUtils;
import com.xplay.xpocker.entity.system.SysPermission;
import com.xplay.xpocker.entity.system.SysUser;
import com.xplay.xpocker.entity.system.SysUserDTO;
import com.xplay.xpocker.mapper.SysPermissionMapper;
import com.xplay.xpocker.mapper.SysUserMapper;
import com.xplay.xpocker.service.system.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xplay.xpocker.util.Assertion;
import com.xplay.xpocker.util.BusinessAssertion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {
    @Autowired
    @Resource
    private SysUserMapper userMapper;
    @Autowired
    @Resource
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public SysUserDTO queryUserInfoByUserName(String userName) {

        SysUser sysUser = userMapper.selectByName(userName);
        Assertion.isNull(new BusinessException("用户名不存在"), sysUser);
        SysUserDTO userDTO = new SysUserDTO();
        List<GrantedAuthority> authorities = null;
        // 2.查询该用户信息权限
        if (sysUser != null) {
            List<SysPermission> permissions = sysPermissionMapper.queryUserPermission(sysUser.getId());
            if (permissions != null && permissions.size() > 0) {
                authorities = new ArrayList<GrantedAuthority>();
                userDTO.setAuthoritiesList(permissions.stream().map(SysPermission::getCode).collect(Collectors.toList()));
                for (SysPermission permission : permissions) {
                    // 添加用户权限
                    authorities.add(new SimpleGrantedAuthority(permission.getCode()));
                }
            }
        }
        userDTO.setSysUser(sysUser);
        userDTO.setGrantedAuthorities(authorities);
        return userDTO;
    }

    @Override
    @Transactional
    public void addUser(SysUser sysUser) {
        SysUser userInfo = new SysUser().selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, sysUser.getUsername()));
        BusinessAssertion.isTrue(String.format("用户[%s]已存在", sysUser.getUsername()), userInfo != null);
        sysUser.insert();
    }

    /**
     * 先暂时只更新头像
     *
     * @param sysUser
     */
    @Override
    @Transactional
    public void updateUser(SysUser sysUser) {
        String username = SecurityUtils.getUserDetails().getUsername();
        SysUser userInfo = new SysUser().selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username));
        userInfo.setPortraitUrl(sysUser.getPortraitUrl());
        userInfo.updateById();
    }
}
