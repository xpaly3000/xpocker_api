package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.system.SysRolePermission;
import com.xplay.xpocker.mapper.SysRolePermissionMapper;
import com.xplay.xpocker.service.system.ISysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements ISysRolePermissionService {

}
