package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.system.SysPermission;
import com.xplay.xpocker.mapper.SysPermissionMapper;
import com.xplay.xpocker.service.system.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

}
