package com.xplay.xpocker.service.system.impl;

import com.xplay.xpocker.entity.system.SysUserRole;
import com.xplay.xpocker.mapper.SysUserRoleMapper;
import com.xplay.xpocker.service.system.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
