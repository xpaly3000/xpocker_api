package com.xplay.xpocker.service.system;

import com.xplay.xpocker.entity.system.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
