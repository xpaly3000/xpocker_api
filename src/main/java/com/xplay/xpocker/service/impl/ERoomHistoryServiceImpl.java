package com.xplay.xpocker.service.impl;

import com.xplay.xpocker.entity.room.ERoomHistory;
import com.xplay.xpocker.mapper.ERoomHistoryMapper;
import com.xplay.xpocker.service.IERoomHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
@Service
public class ERoomHistoryServiceImpl extends ServiceImpl<ERoomHistoryMapper, ERoomHistory> implements IERoomHistoryService {

}
