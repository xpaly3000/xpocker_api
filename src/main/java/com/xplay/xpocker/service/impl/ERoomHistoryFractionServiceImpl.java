package com.xplay.xpocker.service.impl;

import com.xplay.xpocker.entity.room.ERoomHistoryFraction;
import com.xplay.xpocker.mapper.ERoomHistoryFractionMapper;
import com.xplay.xpocker.service.IERoomHistoryFractionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
@Service
public class ERoomHistoryFractionServiceImpl extends ServiceImpl<ERoomHistoryFractionMapper, ERoomHistoryFraction> implements IERoomHistoryFractionService {

}
