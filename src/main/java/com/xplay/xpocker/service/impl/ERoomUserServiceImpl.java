package com.xplay.xpocker.service.impl;

import com.xplay.xpocker.entity.room.ERoomUser;
import com.xplay.xpocker.mapper.ERoomUserMapper;
import com.xplay.xpocker.service.IERoomUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
@Service
public class ERoomUserServiceImpl extends ServiceImpl<ERoomUserMapper, ERoomUser> implements IERoomUserService {

}
