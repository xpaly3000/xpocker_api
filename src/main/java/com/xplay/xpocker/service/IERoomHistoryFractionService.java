package com.xplay.xpocker.service;

import com.xplay.xpocker.entity.room.ERoomHistoryFraction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mr.wan
 * @since 2021-06-03
 */
public interface IERoomHistoryFractionService extends IService<ERoomHistoryFraction> {

}
