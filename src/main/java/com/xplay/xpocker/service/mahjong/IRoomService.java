package com.xplay.xpocker.service.mahjong;

import com.xplay.xpocker.meta.room.MetaRoomEntity;
import com.xplay.xpocker.meta.user.MetaUserChannel;

/**
 * @author wanjie
 * @date 2021/3/28 16:21
 */

public interface IRoomService {

    /**
     * 查询房间 元数据
     *
     * @param roomCode
     * @return
     */
    public MetaRoomEntity queryRoomInfo(String roomCode);

    /**
     * 删除
     *
     * @param roomCode
     * @return
     */
    public void delRoomInfo(String roomCode);

    /**
     * 保存房间
     *
     * @param roomEntity
     * @return
     */
    public void saveRoomInfo(MetaRoomEntity roomEntity);

    /**
     * 在房间内 将用户 设置为 离线
     *
     * @param roomCode
     * @param userId
     * @param online
     * @return
     */

    public void roomUserOnline(String roomCode, String userId, boolean online);

    /**
     * 添加进房间
     *
     * @param roomCode
     * @param userId
     * @return
     */

    public MetaRoomEntity addRoom(String roomCode, String userId);

    /**
     * 查询用户所在房间
     * @param userId
     * @return
     */

    public MetaRoomEntity queryUserRoom(String userId);

    /**
     * 退出房间
     * @param userId
     */

    public void exitRoom(String userId);
}
