package com.xplay.xpocker.service.mahjong;

import com.xplay.xpocker.entity.TMRoomLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-25
 */
public interface ICRoomLogService extends IService<TMRoomLog> {

}
