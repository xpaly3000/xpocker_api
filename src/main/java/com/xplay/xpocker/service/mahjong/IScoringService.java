package com.xplay.xpocker.service.mahjong;

import com.xplay.xpocker.entity.mahjong.CardEnum;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;

/**
 * @author wanjie
 * @date 2021/4/13 22:34
 *
 * 计分接口
 */


public interface IScoringService {
    public void cardBar(MahjongRoomEntity roomInfo,boolean redouble,String userId, CardEnum cardEnum);
    public void cardHu(MahjongRoomEntity roomInfo,boolean redouble,String userId);
}
