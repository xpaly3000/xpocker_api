package com.xplay.xpocker.service.mahjong.impl;

import com.xplay.xpocker.entity.mahjong.CardEnum;
import com.xplay.xpocker.entity.mahjong.MahjongBillBuilder;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IScoringService;
import com.xplay.xpocker.util.CardUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wanjie
 * @date 2021/4/13 23:04
 */
@Service
public class WzScoringServiceImpl implements IScoringService {
    private final Integer CARD_DIANGANG_BAR_SCORING = 3;
    private final Integer CARD_ANGANG_BAR_SCORING = 2;
    private final Integer CARD_MINGGANG_BAR_SCORING = 1;
    // 默认倍数
    private final Integer DEFAULT_REDOUBLE = 2;

    @Override
    public void cardBar(MahjongRoomEntity roomInfo, boolean redouble, String userId, CardEnum cardEnum) {
        Integer redoubleNumber = DEFAULT_REDOUBLE;
        List<MahjongUserChannel> userChannels = roomInfo.notWinnerUsers();
        MahjongUserChannel addUser = roomInfo.getUserInfoByUserId(userId);
        if (!redouble) {
            redoubleNumber--;
        }
        if (cardEnum.equals(CardEnum.DIANGANG)) {
            MahjongUserChannel delUser = roomInfo.getUserInfoByUserId(roomInfo.getCurrentExportUserId());
            roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_BAR, CardUtil.ACTION_DIAN_BAR + String.valueOf(-CARD_DIANGANG_BAR_SCORING * redoubleNumber), delUser.getUserId(), addUser.getUserId(), -CARD_DIANGANG_BAR_SCORING * redoubleNumber).build());
            roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_BAR, CardUtil.ACTION_DIAN_BAR + "+" + String.valueOf(CARD_DIANGANG_BAR_SCORING * redoubleNumber), addUser.getUserId(), delUser.getUserId(), CARD_DIANGANG_BAR_SCORING * redoubleNumber).build());

        } else if (cardEnum.equals(CardEnum.ANGANG) || cardEnum.equals(CardEnum.MINGGANG)) {
            for (MahjongUserChannel delUser : userChannels) {
                if (!delUser.getUserId().equals(userId) && !delUser.isWin()) {
                    String barType = cardEnum.equals(CardEnum.ANGANG) ? CardUtil.ACTION_AN_BAR : CardUtil.ACTION_MING_BAR;
                    int fraction = (cardEnum.equals(CardEnum.ANGANG) ? CARD_ANGANG_BAR_SCORING : CARD_MINGGANG_BAR_SCORING);
                    roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_BAR, barType + String.valueOf(-fraction * redoubleNumber), delUser.getUserId(), addUser.getUserId(), -fraction * redoubleNumber).build());
                    roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_BAR, barType + "+" + String.valueOf(fraction * redoubleNumber), addUser.getUserId(), delUser.getUserId(), fraction * redoubleNumber).build());
                }
            }
        }

    }

    @Override
    public void cardHu(MahjongRoomEntity roomInfo, boolean redouble, String userId) {

    }
}
