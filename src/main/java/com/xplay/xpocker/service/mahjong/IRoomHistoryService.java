package com.xplay.xpocker.service.mahjong;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xplay.xpocker.entity.RoomInfoVO;
import org.apache.ibatis.annotations.Param;

/**
 * @author wanjie
 * @date 2021/6/4 0:13
 */


public interface IRoomHistoryService {
    // 查询指定指定用户的战绩信息
    IPage<RoomInfoVO> queryUserHistoryRoom(Page<RoomInfoVO> pageInfo, @Param(value = "userId") Integer userId);
}
