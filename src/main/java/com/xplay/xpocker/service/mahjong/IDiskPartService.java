package com.xplay.xpocker.service.mahjong;

import com.xplay.xpocker.entity.mahjong.MahjongCardBarEntity;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.meta.user.MetaUserChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wanjie
 * @date 2021/3/23 11:48
 */

public interface IDiskPartService {

    // 获取房间信息
    public MahjongRoomEntity getRoomInfo(String code);


    // 房间发牌
    public List<Integer> licensingCard(int size, MahjongRoomEntity roomInfo, String userId);

    // 当前玩家是否可以执行
    public MahjongUserTask userHasTask(MahjongRoomEntity roomInfo, String userId, String action);

    // 移除用户房间行为
    public boolean removeTask(MahjongRoomEntity roomInfo, String userId);


    // 添加房间行为
    public boolean saveRoom(MahjongRoomEntity roomEntity);

    // 玩家出牌
    public void exportCard(Integer exportCard, MahjongRoomEntity roomInfo, String userSeq);

    /**
     * @param roomInfo
     * @param userChannel
     */
    public void touchCard(MahjongRoomEntity roomInfo, MetaUserChannel userChannel);

    /**
     * @param roomInfo
     * @param userChannel
     * @param doAction    是否确认执行
     */
    public MahjongCardBarEntity barCard(MahjongRoomEntity roomInfo, MetaUserChannel userChannel, Integer cardId, Boolean doAction);
}
