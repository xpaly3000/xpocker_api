package com.xplay.xpocker.message;

import java.lang.annotation.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RoomAnnotation {
    String roomTye() default "";
}
