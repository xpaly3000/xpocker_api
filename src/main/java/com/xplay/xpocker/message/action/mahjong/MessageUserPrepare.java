package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = USER_PREPARE, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageUserPrepare extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageUserPrepare.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {

        // 准备一个 移除一个 最后一个准备完毕 就开始游戏
        MahjongUserTask task = roomInfo.getUserTask(userChannel.getUserId());
        task.setDealWith(true);
        task.setRemoveTag(false);
        userChannel.setPrepare(true);
        // 如果只剩下最后一个任务  而且最后一个任务是当前用户的任务
        if (roomInfo.notDealWithTask() == 0) {
            // 重新初始化房间
            roomInfo.reInit();
            roomInfo.setUserGameTypeStart();
            roomInfo.setBillClearingInfo(null);
            roomInfo.setCurrentGameBillHistory(null);
            roomInfo.setRoomTask(null);
            // 开始游戏 并且发牌
            startGame(roomInfo, diskPartService);
            updateRoomInfo(roomInfo);
            return;
        }
        diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>().initActionAndUId(USER_PREPARE, userChannel.getUserId()), userChannel.getUserId());
    }
}
