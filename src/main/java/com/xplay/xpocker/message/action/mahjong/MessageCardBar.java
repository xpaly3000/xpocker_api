package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.mahjong.CardEnum;
import com.xplay.xpocker.entity.mahjong.MahjongCardBarEntity;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = CARD_BAR, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardBar extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardBar.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;


    // 手牌杠  以及被人打出来的牌杠

    /**
     * 当用户暗杠或者明杠的时候需要指定body  为杠的那张牌
     *
     * @param message
     * @param roomInfo
     */

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        Integer barCard = null;
        try {
            barCard = Integer.valueOf(message.getBody().toString());
        } catch (Exception e) {
            log.info("user not in card info");
        }
        boolean loadingDealWith = false;
        ArrayList<MahjongUserTask> huTask = getHuTask(roomInfo, userChannel.getUserId(), null);
        if (huTask == null || huTask.size() == 0) {
            diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>().initActionAndUId(CARD_BAR, userChannel.getUserId()), null);
            roomInfo.setCurrentBarUser(userChannel);
            MahjongCardBarEntity cardBarEntity = diskPartService.barCard(roomInfo, userChannel, barCard, false);
            if (cardBarEntity.getCardEnum().equals(CardEnum.MINGGANG)) {
                for (Map.Entry<String, MahjongUserChannel> entry : roomInfo.getAllUserInfo().entrySet()) {
                    if (!entry.getKey().equals(userChannel.getUserId())) {
                        MahjongUserChannel checkUser = roomInfo.getUserInfoByUserId(entry.getKey());
                        if (!loadingDealWith) {
                            loadingDealWith = checkUserHuTouchBar(roomInfo, null, checkUser, cardBarEntity.getCardId(), false) != null;
                        } else {
                            checkUserHuTouchBar(roomInfo, null, checkUser, cardBarEntity.getCardId(), false);
                        }
                    }
                }
            }
            if (loadingDealWith) {
                // 当有玩家可以胡牌的时候  这个时候不能去移除  碰牌任务
                MahjongUserTask userTask = roomInfo.getUserTask(userChannel.getUserId());
                userTask.setDealWith(true);
                userTask.setRemoveTag(false);
                roomInfo.setCurrentExportUserId(userChannel.getUserId());
                roomInfo.setCurrentExportCard(cardBarEntity.getCardId());
            } else {
                // 在没玩家胡牌的情况下
                roomInfo.setCurrentBarUser(null);
                diskPartService.barCard(roomInfo, userChannel, barCard, true);
                roomInfo.cleanUserTask(userChannel.getUserId());
                updateRoomInfo(roomInfo);
            }
            licensingCard(true, userChannel.getSeq(), roomInfo, loadingDealWith, diskPartService, true);
        } else {
            // 当有玩家可以胡牌的时候  这个时候不能去移除  碰牌任务
            MahjongUserTask userTask = roomInfo.getUserTask(userChannel.getUserId());
            userTask.setDealWith(false);
        }


    }
}
