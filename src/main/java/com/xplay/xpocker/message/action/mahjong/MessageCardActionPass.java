package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = ACTION_PASS, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardActionPass extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardActionPass.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private MessageCardBar messageCardBar;
    @Autowired
    private MessageCardTouch messageCardTouch;
    @Autowired
    private MessageCardHu messageCardHu;


    // 手牌杠  以及被人打出来的牌杠

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        ArrayList<MahjongUserTask> otherUserTask = roomInfo.getOtherUserTask(userChannel.getUserId());
        roomInfo.getUserTask(userChannel.getUserId()).setDealWith(true);
        if (roomInfo.getRoomTask().size() == 1) {
            if (roomInfo.getExportCardCount() == 0) {
                roomInfo.setRoomTask(null);
                addBankerExport(roomInfo);
                return;
            }
            diskPartService.removeTask(roomInfo, userChannel.getUserId());
            // 将用户的  过  这个行为给移除掉  因为前面有可能会添加 杠 和 胡牌
            MetaUserChannel prevUser = roomInfo.getUserInfoByUserId(roomInfo.getCurrentExportUserId());
            roomInfo.setRoomTask(null);
            Integer nextSeq = licensingCard(false, prevUser.getSeq(), roomInfo, false, diskPartService, true);
        } else if (roomInfo.getRoomTask().size() == 2 && otherUserTask.size() == 1 && otherUserTask.get(0).isDealWith()) {
            // 当任务只剩下两个的时候   并且另外一个任务已经处理了
            /**
             * 首先移除 本体任务  因为   碰撞   操作会判断是否还有 胡的行为
             */
            diskPartService.removeTask(roomInfo, userChannel.getUserId());
            // 在只剩下两个情况下  可能会存在别人  可以胡牌 但是点了过  这个时候需要取出来 继续执行
            MahjongUserTask task = otherUserTask.get(0);
            task.setDealWith(false);
            MahjongUserChannel doTaskUserChannel = roomInfo.getUserInfoByUserId(task.getUserId());
            if (task.getDoTask().equals(CARD_BAR)) {
                roomInfo.setCurrentBarUser(null);
                diskPartService.barCard(roomInfo, doTaskUserChannel, task.getCardId(), true);
                diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>().initActionAndUId(CARD_BAR,userChannel.getUserId()), null);
                updateRoomInfo(roomInfo);
            } else if (task.getDoTask().equals(CARD_TOUCH)) {
                messageCardTouch.doMessage(doTaskUserChannel, new MessageContent(task.getDoTask()), roomInfo);
            } else if (task.getDoTask().equals(CARD_HU)) {
                messageCardHu.doMessage(doTaskUserChannel, new MessageContent(task.getDoTask()), roomInfo);
            }
        }
    }
}
