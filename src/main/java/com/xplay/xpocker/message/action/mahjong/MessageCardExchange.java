package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.mahjong.MahjongCardExchangeInfo;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.BusinessAssertion;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.CardUtil;
import com.xplay.xpocker.util.RedisUtil;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */

@MessageAnnotation(action = EXCHANGE_CARD, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardExchange extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardExchange.class);

    @Autowired
    private MessageDiskDice messageDiskDice;
    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        /**
         * 用户棋牌交换
         * 1. 只能是同一门
         * 2. 如果用户买牌 可以不校验同一门
         * 2-6 对  1-5 右   3-4  左     4个玩家
         * 任何数字对换     2个玩家
         * 偶数左  奇数 右     3个玩家
         *
         * 必须要4个拿出来完了 才能去做服务交换代码
         */
        BusinessAssertion.isNull("非法交换", message.getBody());
        roomInfo.getUserTask(userChannel.getUserId()).setDealWith(true);
        HashMap<String, CopyOnWriteArrayList<Integer>> exchangeCards = roomInfo.getUserExchangeCards();
        if (null == exchangeCards) {
            exchangeCards = new HashMap<String, CopyOnWriteArrayList<Integer>>();
            roomInfo.setUserExchangeCards(exchangeCards);
        }

        String[] userChangeArray = message.getBody().toString().split(",");
        BusinessAssertion.isTrue("非法交换", userChangeArray.length != 3);
        CopyOnWriteArrayList<Integer> userChangeArrayTemp = new CopyOnWriteArrayList<>();
        for (String cardStr : userChangeArray) {
            userChangeArrayTemp.add(Integer.valueOf(cardStr));
        }
        userChannel.setSwap(true);
        CopyOnWriteArrayList<Integer> userCards = roomInfo.getUserCards().get(userChannel.getUserId());
        // 校验用户是否有这3张牌
        BusinessAssertion.isTrue("非法交换", !userCards.contains(userChangeArrayTemp.get(0)) || !userCards.contains(userChangeArrayTemp.get(1))
                || !userCards.contains(userChangeArrayTemp.get(2)));
        exchangeCards.put(userChannel.getUserId(), userChangeArrayTemp);
        /**
         * 将玩家的手牌移除
         */
        CardUtil.removeCard(userCards, roomInfo.getUserExchangeCards().get(userChannel.getUserId()));
        updateRoomInfo(roomInfo);
        if (roomInfo.getRule().getUserCount() == exchangeCards.size()) {
            messageDiskDice.doMessage(userChannel,message,roomInfo);
            // 为庄家添加 摇色子命令
            /*roomInfo.setRoomTask(null);
            MetaUserChannel bankerUser = roomInfo.queryUserBanker();
            roomInfo.addTask(new MahjongUserTask(bankerUser.getUserId(), DISK_DICE, false));
            senMessage(getCtxByUserId(roomInfo.getCode(), bankerUser.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(bankerUser.getUserId()), ACTION_TIPS));*/
        }
    }

    // 使用枚举将情况列举完毕
    private static List<MahjongCardExchangeInfo> makeChangeCardArray(Integer userCount, Integer exchangeValue) {
        List<MahjongCardExchangeInfo> result = new ArrayList<>();
        if (userCount == DictionaryConst.MahjongRoomUserIndexConst.THREE) {
            if (exchangeValue % 2 == 0) {
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.TWO, DictionaryConst.MahjongRoomUserIndexConst.THREE));
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.TWO, null, DictionaryConst.MahjongRoomUserIndexConst.ONE));
            } else {
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.THREE, DictionaryConst.MahjongRoomUserIndexConst.TWO));
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.TWO, null, DictionaryConst.MahjongRoomUserIndexConst.THREE));
            }
            return result;
        } else if (userCount == DictionaryConst.MahjongRoomUserIndexConst.TWO) {
            result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.TWO, DictionaryConst.MahjongRoomUserIndexConst.TWO));
            return result;
        }
        if (exchangeValue == DictionaryConst.MahjongDiceConst.TWO || exchangeValue == DictionaryConst.MahjongDiceConst.SIX) {
            // 交换棋牌  庄家有可能是 1,2,3,4
            if (userCount == DictionaryConst.MahjongRoomUserIndexConst.FOUR) {
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.THREE, DictionaryConst.MahjongRoomUserIndexConst.THREE));
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.TWO, DictionaryConst.MahjongRoomUserIndexConst.FOUR, DictionaryConst.MahjongRoomUserIndexConst.FOUR));
            }
            // 右边
        } else if (exchangeValue == DictionaryConst.MahjongDiceConst.ONE || exchangeValue == DictionaryConst.MahjongDiceConst.FIVE) {
            if (userCount == DictionaryConst.MahjongRoomUserIndexConst.FOUR) {
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.TWO, DictionaryConst.MahjongRoomUserIndexConst.FOUR));
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.THREE, DictionaryConst.MahjongRoomUserIndexConst.FOUR, DictionaryConst.MahjongRoomUserIndexConst.TWO));
            }
        } else {
            if (userCount == DictionaryConst.MahjongRoomUserIndexConst.FOUR) {
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.ONE, DictionaryConst.MahjongRoomUserIndexConst.FOUR, DictionaryConst.MahjongRoomUserIndexConst.TWO));
                result.add(new MahjongCardExchangeInfo(DictionaryConst.MahjongRoomUserIndexConst.THREE, DictionaryConst.MahjongRoomUserIndexConst.TWO, DictionaryConst.MahjongDiceConst.FOUR));
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(makeChangeCardArray(3, 1));
    }
}
