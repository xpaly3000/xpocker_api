package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = CARD_TOUCH, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardTouch extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardTouch.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;


    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        // 当用户执行碰牌的操作的时候这个时候要判断其它用户是否要胡牌 并且 是否做了胡牌的操作
        // 如果其它用户没有胡牌 那么你的碰牌 不能立即生效 考虑双胡 和 单碰的情况
        /**
         * 碰牌需要干的操作
         * 1 移除前一位玩家出的牌
         * 2 添加到自己的 队列中
         */
        // 在没玩家胡牌的情况下
        MetaUserChannel channel = roomInfo.getUserInfoByUserId(userChannel.getUserId());
        ArrayList<MahjongUserTask> huTask = getHuTask(roomInfo, userChannel.getUserId(), null);
        if (huTask == null || huTask.size() == 0) {
            diskPartService.touchCard(roomInfo, channel);
            roomInfo.setCurrentExportUserId(channel.getUserId());
            roomInfo.addNewTask(new MahjongUserTask(userChannel.getUserId(), EXPORT_CARD));
            tipsCallCard(roomInfo, userChannel);
            // 告诉其他人 我碰牌了
            diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>().initActionAndUId(CARD_TOUCH,userChannel.getUserId()), null);
            ArrayList<Integer> barList = roomInfo.checkUserBar(userChannel);
            if (barList != null) {
                roomInfo.addTask(new MahjongUserTask(userChannel.getUserId(), CARD_BAR));
                roomInfo.addTask(new MahjongUserTask(userChannel.getUserId(), EXPORT_CARD));
            }
            updateRoomInfo(roomInfo);
        } else {
            // 当有玩家可以胡牌的时候  这个时候不能去移除  碰牌任务
            MahjongUserTask userTask = roomInfo.getUserTask(userChannel.getUserId());
            userTask.setDealWith(true);
            userTask.setRemoveTag(false);
        }
    }
}
