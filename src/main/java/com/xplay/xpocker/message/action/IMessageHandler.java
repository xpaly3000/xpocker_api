package com.xplay.xpocker.message.action;

import com.xplay.xpocker.meta.MessageContent;
import io.netty.channel.ChannelHandlerContext;

/**
 * 房间消息处理器的顶层接口
 *
 * @author wanjie
 * @date 2021/3/17 21:42
 */

public interface IMessageHandler {
    public void handler(ChannelHandlerContext ctx, MessageContent message);
}
