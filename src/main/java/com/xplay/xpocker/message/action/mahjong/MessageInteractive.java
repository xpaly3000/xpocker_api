package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.mahjong.CardEnum;
import com.xplay.xpocker.entity.mahjong.MahjongCardBarEntity;
import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.MessageToEnum;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.CARD_BAR;
import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.INTERACTIVE;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = INTERACTIVE, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageInteractive extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageInteractive.class);

    /**
     * 这里不需要做 太多的事情  直接转发给所有人
     *
     * @param userChannel
     * @param message
     * @param roomInfo
     */
    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        message.setMessageType(MessageToEnum.ALL);
    }
}
