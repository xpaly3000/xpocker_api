package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.CrRoomInfo;
import com.xplay.xpocker.entity.mahjong.*;
import com.xplay.xpocker.entity.room.ERoomHistory;
import com.xplay.xpocker.entity.room.ERoomHistoryFraction;
import com.xplay.xpocker.entity.room.ERoomUser;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.observer.SubscriptionSubject;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.socket.UserChannelMate;
import com.xplay.xpocker.util.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/26 11:35
 */

public class DiskPartCurrency implements UserChannelMate {
    /**
     * @param roomInfo
     * @param task     不做消息推送 仅仅只是为所有用户添加task
     */

    public void addAllTask(MahjongRoomEntity roomInfo, String task) {
        roomInfo.setRoomTask(null);
        HashMap<String, MahjongUserChannel> userInfo = roomInfo.getAllUserInfo();
        for (Map.Entry<String, MahjongUserChannel> userChannelEntry : userInfo.entrySet()) {
            roomInfo.addTask(new MahjongUserTask(userChannelEntry.getValue().getUserId(), task));
        }
    }

    /**
     * 下一把游戏 并且结算账单
     *
     * @param roomInfo
     * @param notCard
     */

    public void nextGame(MahjongRoomEntity roomInfo, boolean notCard) {
        // 存储 本局 游戏时间信息
        ERoomHistory roomHistory = new ERoomHistory();
        roomHistory.setEndDate(LocalDateTime.now());
        roomHistory.setStartDate(roomInfo.getCurrentGameStartDate());
        roomHistory.setGameNumbers(roomInfo.getGamesCurrentNumber());
        roomHistory.setRoomId(roomInfo.getDbSequence());
        roomHistory.insert();
        int notWinnerSum = roomInfo.notWinnerSum();
        if (notWinnerSum == 1 || notCard) {
            // 清算账单
            roomInfo.setState(DictionaryConst.MahjongRoomState.BILL_CLEARING);
            // 取出所有的 未胡牌玩家
            List<MahjongUserChannel> notWinnerUsers = roomInfo.notWinnerUsers();

            // 赔叫选手
            List<MahjongUserChannel> allClaimUsers = new ArrayList<>();
            // 下叫选手
            List<MahjongUserChannel> nextCallUsers = new ArrayList<>();
            if (notWinnerUsers != null && notWinnerUsers.size() > 1) {
                notWinnerUsers.forEach(source -> {
                    WzHuCardType maxHuType = checkUserMaxType(roomInfo.getUserCards().get(source.getUserId()), roomInfo.getUserAllTouchBarCards(source.getUserId()));
                    if (maxHuType == null) {
                        allClaimUsers.add(source);
                    } else {
                        nextCallUsers.add(source);
                    }
                });
            }
            if (allClaimUsers.size() > 0 && nextCallUsers.size() > 0) {
                for (MahjongUserChannel claimUser : allClaimUsers) {
                    for (MahjongUserChannel callUser : nextCallUsers) {
                        WzHuCardType maxHuType = checkUserMaxType(roomInfo.getUserCards().get(callUser.getUserId()), roomInfo.getUserAllTouchBarCards(callUser.getUserId()));
                        maxHuType.addTurns(callUser.isListening(), claimUser.isListening(), roomInfo.getRule().getRules().contains(DictionaryConst.MahjongRuleConst.SING_UP));
                        roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_CLAIM, maxHuType.getType(), callUser.getUserId(), callUser.getUserId(), maxHuType.getFraction(roomInfo.getRule().getTurns())).build());
                        roomInfo.addBillHistory(new MahjongBillBuilder().init(CardUtil.ACTION_CALLING, maxHuType.getType(), claimUser.getUserId(), callUser.getUserId(), -maxHuType.getFraction(roomInfo.getRule().getTurns())).build());
                    }
                }
            }


            /**
             * 将用户扣分信息 添加到用户对象上
             */
            Set<Map.Entry<String, MahjongUserChannel>> entries = roomInfo.getAllUserInfo().entrySet();
            for (Map.Entry<String, MahjongUserChannel> entry : entries) {
                if (null != roomInfo.getCurrentGameBillHistory()) {
                    for (MahjongBillEntity billEntity : roomInfo.getCurrentGameBillHistory()) {
                        if (billEntity.getSourceId().equals(entry.getKey())) {
                            entry.getValue().setIntegral(entry.getValue().getIntegral() + billEntity.getFraction());
                        }
                    }
                }
            }

            if (null != notWinnerUsers) {
                // 如果没有一个人胡牌  设置最后一个玩家为庄家
                if (notWinnerUsers.size() == roomInfo.getRule().getUserCount()) {
                    roomInfo.setUserBanker(roomInfo.getCurrentExportUserId());
                }
                for (MahjongUserChannel notWinnerUser : notWinnerUsers) {
                    roomInfo.addBillClearingInfo(notWinnerUser, false);
                }
            }
            // 将所有胡牌的玩家 形成战绩
            ArrayList<MahjongBillClearing> billClearingInfo = roomInfo.getBillClearingInfo();
            for (MahjongBillClearing billClearing : billClearingInfo) {
                String userId = billClearing.getUserInfo().getUserId();

                if (billClearing.getUserWinSeq() == 1) {
                    roomInfo.setUserBanker(billClearing.getUserInfo().getUserId());
                }
                billClearing.setUserCards(roomInfo.getUserCards().get(userId));
                billClearing.setHuCard(roomInfo.getUserInfoByUserId(userId).getHuCard());
                billClearing.setCardTouchBars(roomInfo.getUserTouchBar().get(userId));
                if (null != roomInfo.getCurrentGameBillHistory()) {
                    for (MahjongBillEntity billEntity : roomInfo.getCurrentGameBillHistory()) {
                        /**
                         * 这里需要对 杠牌做处理
                         */
                        /**
                         * 当用户是杠牌 然后需要加分的话  需要判断是否胡牌 或者下叫
                         *
                         * 同理  扣钱 也需要判断对方
                         */
                        boolean addBillTag = false;

                        if (billEntity.getType().equals(CardUtil.ACTION_BAR) && billEntity.getFraction() > 0 && (roomInfo.getUserInfoByUserId(billEntity.getSourceId()).isWin() || nextCallUsers.stream().filter(userChannel -> userChannel.getUserId().equals(billEntity.getSourceId())).findAny().isPresent())) {
                            addBillTag = true;
                        }

                        if (billEntity.getType().equals(CardUtil.ACTION_BAR) && billEntity.getFraction() < 0 && (roomInfo.getUserInfoByUserId(billEntity.getTargetId()).isWin() || nextCallUsers.stream().filter(userChannel -> userChannel.getUserId().equals(billEntity.getTargetId())).findAny().isPresent())) {
                            addBillTag = true;
                        }

                        if (!billEntity.getType().equals(CardUtil.ACTION_BAR) && billEntity.getSourceId().equals(billClearing.getUserInfo().getUserId())) {
                            addBillTag = true;
                        }
                        if (addBillTag) {
                            billClearing.getFractionResources().add(billEntity.getAction() + "(" + billEntity.getType() + ")");
                            billClearing.setFraction(billClearing.getFraction() + billEntity.getFraction());
                        }
                    }
                }
            }
            // 存储 本局 的 分数信息
            for (MahjongBillClearing billClearing : billClearingInfo) {
                ERoomHistoryFraction roomHistoryFraction = new ERoomHistoryFraction();
                roomHistoryFraction.setFraction(billClearing.getFraction());
                roomHistoryFraction.setUserId(billClearing.getUserInfo().getSysUser().getId());
                roomHistoryFraction.setUserCard(billClearing.getUserCards().toString());
                roomHistoryFraction.setHistoryId(roomHistory.getId());
                roomHistoryFraction.setFractionInfo(billClearing.getFractionResources().toString());
                roomHistoryFraction.insert();
            }
            if ((roomInfo.getGamesCurrentNumber() + 1) > roomInfo.getRule().getNumberGame()) {
                roomInfo.setGameEndDate(new Date());
                roomInfo.setState(DictionaryConst.MahjongRoomState.OVER);
                updateRoomInfo(roomInfo);
                RedisUtil redisUtil = SpringContextHolder.getBean(RedisUtil.class);
                CrRoomInfo crRoomInfo = new CrRoomInfo();
                crRoomInfo.setId(roomInfo.getDbSequence());
                crRoomInfo.setEndDate(LocalDateTime.now());
                crRoomInfo.updateById();
                redisUtil.del(DictionaryConst.RedisHeader.ROOM_HEADER + roomInfo.getCode());
                return;
            }
            roomInfo.setCurrentGameStartDate(LocalDateTime.now());
            roomInfo.setGamesCurrentNumber(roomInfo.getGamesCurrentNumber() + 1);
            roomInfo.setRoomTask(null);
            // 添加准备队列
            addAllTask(roomInfo, USER_PREPARE);
            // 并通知所有人准备了
            diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>(USER_PREPARE), null);
            updateRoomInfo(roomInfo);
        }

    }


    /**
     * 在没有胡牌的情况下 获取用户最大的牌型
     *
     * @param userCard
     * @param touchBar
     */

    public static WzHuCardType checkUserMaxType(List<Integer> userCard, List<Integer> touchBar) {
        ArrayList<Integer> userHuCard = CardUtil.checkUserHuCard(userCard);
        if (userHuCard == null || userHuCard.size() == 0) {
            return null;
        }
        ArrayList<WzHuCardType> huList = new ArrayList<>();

        for (Integer huCard : userHuCard) {
            ArrayList<Integer> userCardTemp = new ArrayList<>(userCard);
            userCardTemp.add(huCard);
            huList.add(CardUtil.checkCardType(userCardTemp, touchBar));
        }
        huList.sort((a, b) -> b.getFraction().compareTo(a.getFraction()));
        return huList.get(0);
    }


    public void addBankerExport(MahjongRoomEntity roomInfo) {
        // 为庄家添加出牌指令
        for (Map.Entry<String, MahjongUserChannel> userItem : roomInfo.getAllUserInfo().entrySet()) {
            //判断是否为天胡 如果是的话  请一位玩家继续 如果不是的话  添加庄家的发牌指令  等待出牌
            if (userItem.getValue().isBanker()) {
                // 房间添加庄家出牌指令
                if (CardUtil.checkUserHu(roomInfo.getUserCards().get(userItem.getKey()))) {
                    roomInfo.addTask(new MahjongUserTask(userItem.getKey(), CARD_HU, false));
                }
                if (CardUtil.checkCardNumber(roomInfo.getUserCards().get(userItem.getKey()), 4).size() > 0) {
                    roomInfo.addTask(new MahjongUserTask(userItem.getKey(), CARD_BAR, false));
                }
                roomInfo.addTask(new MahjongUserTask(userItem.getKey(), DictionaryConst.MahjongAction.EXPORT_CARD, false));
                promptUserDoAction(roomInfo, userItem.getKey());
            }
        }
    }


    /**
     * userId 传入 就是取其它的 不传入 取所有
     * isDealWith 传入 取 相对应的 处理 或者未处理  不传入  取所有
     *
     * @param roomInfo
     * @param userId
     * @param isDealWith
     * @return
     */

    public ArrayList<MahjongUserTask> getHuTask(MahjongRoomEntity roomInfo, String userId, Boolean isDealWith) {
        boolean result = false;
        ArrayList<MahjongUserTask> roomTask = roomInfo.getRoomTask();
        if (null == roomInfo || roomTask.size() == 0) {
            return null;
        }
        ArrayList<MahjongUserTask> huTaskArray = new ArrayList<>();
        Iterator<MahjongUserTask> iterator = roomTask.iterator();
        while (iterator.hasNext()) {
            MahjongUserTask task = iterator.next();
            CopyOnWriteArrayList<String> taskArray = task.getTaskArray();
            if ((taskArray == null || taskArray.size() == 0) && task.getTask() != null && hasHuType(isDealWith, task.getTask(), task.getUserId(), userId, task.isDealWith())) {
                huTaskArray.add(task);
            } else if (taskArray != null && taskArray.size() > 0) {
                for (String taskItem : taskArray) {
                    if (hasHuType(isDealWith, taskItem, task.getUserId(), userId, task.isDealWith())) {
                        huTaskArray.add(task);
                        break;
                    }

                }
            }
        }
        return huTaskArray;
    }

    public boolean hasHuType(Boolean isDealWith, String action, String sourceUserId, String userId, Boolean sourceDealWith) {
        if (isDealWith == null && action.equals(CARD_HU) && null == userId) {
            return true;
        } else if (isDealWith == null && action.equals(CARD_HU) && !sourceUserId.equals(userId)) {
            return true;
        } else if (isDealWith != null && isDealWith == sourceDealWith && action.equals(CARD_HU) && null == userId) {
            return true;
        } else if (isDealWith != null && isDealWith == sourceDealWith && action.equals(CARD_HU) && null != userId && !sourceUserId.equals(userId)) {
            return true;
        }
        return false;
    }

    /**
     * 开始游戏  并且把发出来的牌 送给 每一个玩家
     * 为庄家添加摇骰子的操作
     *
     * @param roomInfo
     * @param diskPartService
     */

    public void startGame(MahjongRoomEntity roomInfo, IDiskPartService diskPartService) {
        HashMap<String, MahjongUserChannel> userInfo = roomInfo.getAllUserInfo();
        CrRoomInfo crRoomInfo = new CrRoomInfo();
        crRoomInfo.setId(roomInfo.getDbSequence());
        crRoomInfo.setStartDate(LocalDateTime.now());
        crRoomInfo.updateById();
        for (Map.Entry<String, MahjongUserChannel> userChannelEntry : userInfo.entrySet()) {
            // 当游戏还是创建的时候 将用户插入到 e表中
            if (roomInfo.getState() == DictionaryConst.MahjongRoomState.CREATE) {
                ERoomUser roomUser = new ERoomUser();
                roomUser.setRoomId(roomInfo.getDbSequence());
                roomUser.setUserId(userChannelEntry.getValue().getSysUser().getId());
                roomUser.insert();
            }
            MetaUserChannel userChannel = userChannelEntry.getValue();
            if (userChannel.isBanker()) {
                // 这里不能执行  添加任务 因为下面做了房间的状态刷新会覆盖
                roomInfo.setCurrentExportUserId(userChannel.getUserId());
                diskPartService.licensingCard(DictionaryConst.MahjongConstant.BANKER_COUNT, roomInfo, userChannel.getUserId());
            } else {
                diskPartService.licensingCard(DictionaryConst.MahjongConstant.LEISURE_COUNT, roomInfo, userChannel.getUserId());
            }
        }
        addAllTask(roomInfo, EXCHANGE_CARD);
        roomInfo.setCurrentGameStartDate(LocalDateTime.now());
        roomInfo.setState(DictionaryConst.MahjongRoomState.CHANGE);
        //遍历所有的棋牌 然后发给指定的用户 其它用户不可见
        HashMap<String, CopyOnWriteArrayList<Integer>> userCards = roomInfo.getUserCards();
        for (Map.Entry<String, CopyOnWriteArrayList<Integer>> entries : userCards.entrySet()) {
            MetaUserChannel userChannel = roomInfo.getUserInfoByUserId(entries.getKey());
            // 收牌 提示
            //  senMessage(subject.getUserChannelByUserId(entries.getKey()), new MessageContent(roomInfo.getUserCards().get(entries.getKey()).toString(), ACCEPT_DICE));
            // 行为提示
            senMessage(getCtxByUserId(roomInfo.getCode(), userChannel.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(userChannel.getUserId()), ACTION_TIPS));
        }
        log.info(String.format("========房间号============== room [%s] ===========start game======", roomInfo.getCode()));
        roomInfo.setGameStartDate(new Date());
        //roomInfo.setState(DictionaryConst.MahjongRoomState.START);
    }

    /**
     * 这里 还需要处理  前三 后四
     *
     * @param roomInfo
     * @param exportUser 出牌人  如果为空的话  便是发牌
     * @param checkUser  检查人
     * @param inCard
     * @return
     */

    public MahjongUserTask checkUserHuTouchBar(MahjongRoomEntity roomInfo, MahjongUserChannel exportUser, MahjongUserChannel checkUser, Integer inCard, boolean isLicensingCard) {
        // 如果说出牌人  有报叫的话
        // 如果规则里面有报叫
        if (checkUser.isWin()) {
            return null;
        }
        ArrayList<Integer> touchBar = null;
        boolean sendMsgTag = false;
        try {
            touchBar = roomInfo.getUserAllTouchBarCards(checkUser.getUserId());
        } catch (Exception e) {

        }
        CopyOnWriteArrayList<Integer> userCard = roomInfo.getUserCards().get(checkUser.getUserId());
        ArrayList<Integer> userCardTemp = new ArrayList<>();
        userCardTemp.addAll(userCard);
        /**
         * 如果用户报叫的话 那么就是必胡了
         */
        if (checkUser.isListening()) {
            // 如果不是自己摸得牌  对方打的牌 需要放进来
            boolean huCardTag = false;
            if (!isLicensingCard) {
                userCardTemp.add(inCard);
            }
            ArrayList<Integer> callHuCards = checkUser.getCallHuCards();
            WzHuCardType huCardType = CardUtil.checkCardType(userCardTemp, touchBar);
            if (null != huCardType) {
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_HU));
                senMessage(getCtxByUserId(roomInfo.getCode(), checkUser.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(checkUser.getUserId()), ACTION_TIPS));
                huCardTag = true;
            }
            // 首先检查是否能杠 如果可以  移除 手上的牌    再检查胡牌
            ArrayList<Integer> barList = CardUtil.checkUserBar(userCardTemp, roomInfo.getUserAllTouchBarCards(checkUser.getUserId()));
            boolean barTag = false;
            if (barList != null) {
                for (Integer barItem : barList) {
                    int frequencyCount = Collections.frequency(userCardTemp, barItem);
                    // 移除元素过后  检测是否还能胡牌
                    userCardTemp.remove(barItem);
                    ArrayList<Integer> huCards = CardUtil.checkUserHuCard(userCardTemp);
                    if(huCards.size()==0){
                        break;
                    }
                    if (callHuCards.containsAll(huCards) && huCards.containsAll(callHuCards)) {
                        barTag = true;
                        roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_BAR));
                    }
                    for (int i = 0; i < frequencyCount; i++) {
                        userCardTemp.add(barItem);
                    }
                }
            }
            if (barTag && isLicensingCard) {
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), EXPORT_CARD));
            }
            if (barTag && !isLicensingCard) {
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), ACTION_PASS));
            }
            if (huCardTag || barTag) {
                senMessage(getCtxByUserId(roomInfo.getCode(), checkUser.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(checkUser.getUserId()), ACTION_TIPS));
            }
            return roomInfo.getUserTask(checkUser.getUserId());
        }
        // 判断用户是否  碰  胡  杠 如果是的 添加到房间的任务队列中
        if (!isLicensingCard) {
            // 如果是别人出的牌的话判断别人是否碰牌
            if (CardUtil.checkCardTouch(userCardTemp, inCard)) {
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_TOUCH));
            }
            // 如果是别人出的牌的话判断别人是否碰牌
            if (Collections.frequency(userCardTemp, inCard) == 3) {
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_BAR));
            }
        } else {
            if (Collections.frequency(userCardTemp, inCard) == 4) {
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_BAR));
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), EXPORT_CARD));
            }
            ArrayList<Integer> barList = roomInfo.checkUserBar(checkUser);
            if (barList != null) {
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_BAR));
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), EXPORT_CARD));
            }
        }
        if (!isLicensingCard) {
            userCardTemp.add(inCard);
        }
        WzHuCardType huCardType = CardUtil.checkCardType(userCardTemp, touchBar);
        if (null != huCardType) {
            if (isLicensingCard) {
                // 如果是摸的牌添加胡牌行为
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_HU));
            } else if (roomInfo.getCurrentBarUser() != null) {
                // 如果是别人出的牌 并且是杠上打出来的 直接添加
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_HU));
            } else if (exportUser != null && exportUser.isListening()) {
                // 如果是别人出的牌 然后别人 又是报叫状态
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_HU));
            } else if (huCardType.getTurns() > CardUtil.ZERO) {
                // 如果是别人出的牌 并且是不是杠上打出来的 就需要翻数大于一
                sendMsgTag = true;
                roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), CARD_HU));
            }
        }
        if (sendMsgTag && !isLicensingCard) {
            // 添加过的 行为
            roomInfo.addTask(new MahjongUserTask(checkUser.getUserId(), ACTION_PASS));
        }
        if (sendMsgTag) {
            senMessage(getCtxByUserId(roomInfo.getCode(), checkUser.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(checkUser.getUserId()), ACTION_TIPS));
        }
        return roomInfo.getUserTask(checkUser.getUserId());
    }

    /**
     * 检测用户报叫
     *
     * @param roomInfo
     * @param userChannel
     */

    public void tipsCallCard(MahjongRoomEntity roomInfo, MahjongUserChannel userChannel) {
        if (roomInfo.getRule().getRules().contains(DictionaryConst.MahjongRuleConst.SING_UP) && roomInfo.getExportCardCount() <= 3 && !userChannel.isListening()) {
            List<Integer> exportByCall = CardUtil.checkUserCallCard(roomInfo.getCurrentUserCard(userChannel.getUserId()));
            if (exportByCall != null && exportByCall.size() > 0) {
                roomInfo.addTask(new MahjongUserTask(userChannel.getUserId(), CARD_CALL));
                senMessage(getCtxByUserId(roomInfo.getCode(), userChannel.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(userChannel.getUserId()), ACTION_TIPS, userChannel.getUserId()));
            }
        }
    }

    /**
     * 通用发牌操作
     *
     * @param isCurrent       是否为当前用户 如果是当前用户的话将不执行找下一个玩家是谁
     * @param seq             最后一次执行出牌的玩家
     * @param roomInfo        房间信息
     * @param loadingDealWith 是否需要等待
     * @param diskPartService 桌面服务
     * @param isLicensingCard 判断是否为出牌操作或者摸牌操作
     */

    public Integer licensingCard(boolean isCurrent, Integer seq, MahjongRoomEntity roomInfo, boolean loadingDealWith, IDiskPartService diskPartService, boolean isLicensingCard) {
        if (!loadingDealWith) {
            // 执行发牌操作
            boolean tag = true;
            Integer nextUserSeq = seq;
            while (tag) {
                if (!isCurrent) {
                    nextUserSeq = nextUserSeq + 1;
                    // 如果大于玩家的总数 从1继续开始
                    if (nextUserSeq > roomInfo.getRule().getUserCount()) {
                        nextUserSeq = 1;
                    }
                }
                MahjongUserChannel nextUserChannel = roomInfo.getUserInfoBySeq(nextUserSeq);
                if (!nextUserChannel.isWin()) {
                    roomInfo.setCurrentExportUserId(nextUserChannel.getUserId());
                    if (roomInfo.notWinnerSum() <= 1) {
                        nextGame(roomInfo, true);
                        return null;
                    }
                    List<Integer> licensingCard = diskPartService.licensingCard(1, roomInfo, nextUserChannel.getUserId());
                    if (null == licensingCard || licensingCard.size() == 0) {
                        nextGame(roomInfo, true);
                        return null;
                    }
                    roomInfo.setLicensingCard(licensingCard.get(0));
                    tipsCallCard(roomInfo, nextUserChannel);
                    MahjongUserTask userTask = checkUserHuTouchBar(roomInfo, null, nextUserChannel, licensingCard.get(0), isLicensingCard);
                    if ((nextUserChannel.isListening() && userTask == null) || !nextUserChannel.isListening()) {
                        roomInfo.addTask(new MahjongUserTask(nextUserChannel.getUserId(), DictionaryConst.MahjongAction.EXPORT_CARD));
                    }
                    senMessage(getCtxByUserId(roomInfo.getCode(), nextUserChannel.getUserId()), new MessageContent<MahjongUserTask>(roomInfo.getUserTask(nextUserChannel.getUserId()), ACTION_TIPS, nextUserChannel.getUserId()));
                    senMessage(getCtxByUserId(roomInfo.getCode(), nextUserChannel.getUserId()), new MessageContent<LicensingCard>(new LicensingCard(licensingCard.get(0), userTask), CARD_LICENSING, nextUserChannel.getUserId()));
                    // 告诉其它人 摸牌了
                    diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<String>().initActionAndUId(CARD_LICENSING, nextUserChannel.getUserId()), nextUserChannel.getUserId());
                    return nextUserSeq;
                } else {
                    continue;
                }
            }
        }
        return null;
    }
}
