package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.business.BusinessException;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.BusinessAssertion;
import com.xplay.xpocker.util.CardUtil;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = EXPORT_CARD, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardExport extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardExport.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        roomInfo.getUserTask(userChannel.getUserId()).setDealWith(true);

        Integer exportCard = null;
        try {
            exportCard = Integer.valueOf(message.getBody().toString());
        } catch (Exception e) {
            throw new BusinessException("用户出牌违规");
        }

        HashMap<String, CopyOnWriteArrayList<Integer>> userCards = roomInfo.getUserCards();
        BusinessAssertion.isTrue("出牌非法", !userCards.get(userChannel.getUserId()).contains(exportCard));
        MetaUserChannel memoryUserChannel = roomInfo.getUserInfoByUserId(userChannel.getUserId());
        roomInfo.setCurrentExportCard(exportCard);
        // 当摸牌的玩家不是杠牌玩家  那么删除杠的用户
        MahjongUserChannel barUser = roomInfo.getCurrentBarUser();
        if (barUser != null && !barUser.getUserId().equals(userChannel.getUserId())) {
            roomInfo.setCurrentBarUser(null);
        }
        // 将用户出的牌添加到出牌队列
        diskPartService.exportCard(exportCard, roomInfo, memoryUserChannel.getUserId());
        if (userChannel.isListening() && userChannel.getCallHuCards() == null) {
            ArrayList<Integer> huCards = CardUtil.checkUserHuCard(userCards.get(userChannel.getUserId()));
            BusinessAssertion.isTrue("出牌非法", huCards == null || huCards.size() == 0);
            userChannel.setCallHuCards(huCards);
        }
        roomInfo.setCurrentExportUserId(userChannel.getUserId());
        boolean loadingDealWith = false;
        Set<Map.Entry<String, CopyOnWriteArrayList<Integer>>> entries = userCards.entrySet();
        roomInfo.setRoomTask(null);
        for (Map.Entry<String, CopyOnWriteArrayList<Integer>> entry : entries) {
            if (!entry.getKey().equals(memoryUserChannel.getUserId())) {
                MahjongUserChannel checkUser = roomInfo.getUserInfoByUserId(entry.getKey());
                if (!loadingDealWith) {
                    loadingDealWith = checkUserHuTouchBar(roomInfo, userChannel, checkUser, exportCard, false) != null;
                } else {
                    checkUserHuTouchBar(roomInfo, userChannel, checkUser, exportCard, false);
                }
            }
        }
        diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent<Integer>(exportCard, EXPORT_CARD, userChannel.getUserId()), userChannel.getUserId());
        // 如果没有人处理 执行发牌操作
        licensingCard(false, memoryUserChannel.getSeq(), roomInfo, loadingDealWith, diskPartService, true);

    }

}
