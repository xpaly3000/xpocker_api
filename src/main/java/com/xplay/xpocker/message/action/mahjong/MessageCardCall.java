package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.util.CardUtil;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CopyOnWriteArrayList;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.*;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = CARD_CALL, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageCardCall extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageCardCall.class);

    @Autowired
    private IDiskPartService diskPartService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 手牌  如果余 2 的话  就要出牌   如果不余 2 的话
     *
     * @param userChannel
     * @param message
     * @param roomInfo
     */

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        userChannel.setListening(true);
        CopyOnWriteArrayList<Integer> userCards = roomInfo.getUserCards().get(userChannel.getUserId());
        if (userCards.size() % 3 != 2) {
            userChannel.setCallHuCards(CardUtil.checkUserHuCard(userCards));
        }
        if (roomInfo.getExportCardCount() == 0 && roomInfo.getRoomTask().size() == 1) {
            roomInfo.getUserTask(userChannel.getUserId()).setDealWith(true);
            roomInfo.setRoomTask(null);
            addBankerExport(roomInfo);
        } else if (userCards.size() % 3 == 2) {
            // 这里需要注意一下 当设置任务被处理  后续会删除当前用户的任务  所以这里直接移除用户的行为然后添加出牌操作
            roomInfo.cleanUserTask(userChannel.getUserId());
            roomInfo.addTask(new MahjongUserTask(userChannel.getUserId(), EXPORT_CARD, false));
            promptUserDoAction(roomInfo, userChannel.getUserId());
        }else{
            roomInfo.getUserTask(userChannel.getUserId()).setDealWith(true);
        }
        diskPart.get(roomInfo.getCode()).notifyMessage(new MessageContent().initActionAndUId(CARD_CALL, userChannel.getUserId()), null);
    }
}
