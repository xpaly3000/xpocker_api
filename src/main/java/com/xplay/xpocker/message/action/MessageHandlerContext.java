package com.xplay.xpocker.message.action;

import com.xplay.xpocker.util.SpringContextHolder;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wanjie
 * @date 2021/3/17 21:49
 */

public class MessageHandlerContext {
    private ConcurrentHashMap<String, ConcurrentHashMap<String, Class>> handlerMap;

    public MessageHandlerContext(ConcurrentHashMap<String, ConcurrentHashMap<String, Class>> handlerMap) {
        this.handlerMap = handlerMap;
    }

    public IMessageHandler getInstance(String key, String action) {
        ConcurrentHashMap<String, Class> connectStrategyMap = handlerMap.get(key);
        if (null == connectStrategyMap) {
            throw new IllegalArgumentException("clazz map is not found");
        }
        Class connectStrategy = connectStrategyMap.get(action);
        if (null == connectStrategy) {
            throw new IllegalArgumentException("clazz is not found");
        }
        return (IMessageHandler) SpringContextHolder.getBean(connectStrategy);
    }
}
