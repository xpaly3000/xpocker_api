package com.xplay.xpocker.message.action.mahjong;

import com.xplay.xpocker.entity.mahjong.MahjongUserTask;
import com.xplay.xpocker.message.MessageToEnum;
import com.xplay.xpocker.message.action.MessageAnnotation;
import com.xplay.xpocker.message.action.MessageStrategy;
import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IDiskPartService;
import com.xplay.xpocker.service.mahjong.IRoomService;
import com.xplay.xpocker.util.BusinessAssertion;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.LOG_OUT;
import static com.xplay.xpocker.util.DictionaryConst.MahjongAction.USER_PREPARE;

/**
 * @author wanjie
 * @date 2021/3/17 21:43
 */
@MessageAnnotation(action = LOG_OUT, type = {DictionaryConst.RoomTypeConst.WZ_MAHJONG})
@Service
public class MessageLogout extends MessageStrategy {
    private static final Logger log = LoggerFactory.getLogger(MessageLogout.class);
    @Autowired
    private IRoomService roomService;

    @Override
    public void doMessage(MahjongUserChannel userChannel, MessageContent message, MahjongRoomEntity roomInfo) {
        //如果游戏不是创建状态那么禁止退出游戏
        BusinessAssertion.isTrue("游戏已开始禁止退出", roomInfo.getState() != DictionaryConst.MahjongRoomState.CREATE);
        roomInfo.removeUser(userChannel.getUserId());
        roomService.exitRoom(userChannel.getUserId());
        roomInfo.setRoomTask(null);
        roomInfo.getAllUserInfo().forEach((uName, uInfo) -> {
            uInfo.setPrepare(false);
        });
        updateRoomInfo(roomInfo);
        // 消息回调
        message.setMessageType(MessageToEnum.CURRENT);
    }
}
