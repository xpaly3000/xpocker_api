package com.xplay.xpocker.message.connecter;

import com.xplay.xpocker.util.SpringContextHolder;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wanjie
 * @date 2021/3/17 21:49
 */

public class ConnectHandlerContext {
    private ConcurrentHashMap<String, Class> handlerMap;

    public ConnectHandlerContext(ConcurrentHashMap<String, Class> handlerMap){
        this.handlerMap = handlerMap;
    }

    public ConnectStrategy getInstance(String key) {
        Class connectStrategy = handlerMap.get(key);
        if (null == connectStrategy) {
            throw new IllegalArgumentException("clazz is not found");
        }
        return (ConnectStrategy) SpringContextHolder.getBean(connectStrategy);
    }
}
