package com.xplay.xpocker.controller;

import com.xplay.xpocker.business.ResultFul;
import com.xplay.xpocker.config.security.VerificationUtil;
import com.xplay.xpocker.entity.system.VerificationMode;
import com.xplay.xpocker.util.Assertion;
import com.xplay.xpocker.util.BusinessAssertion;
import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author wanjie
 * @date 2021/4/20 20:15
 */
@RestController
@RequestMapping(value = "/verification", produces = "application/json;charset=UTF-8")
public class VerificationController {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private VerificationUtil verificationUtil;

    private Logger logger = LoggerFactory.getLogger(VerificationController.class);


    // 控制滑动验证码  的误差
    private final double CODE_MOVE_GAY = 0.05f;


    @GetMapping(value = "generateCode")
    private ResultFul<VerificationMode> generateCode() {
        VerificationMode verificationMode = verificationUtil.generateVli();
        VerificationMode redisObject = new VerificationMode();
        BeanUtils.copyProperties(verificationMode, redisObject);
        redisObject.setBackGroundImage(null);
        redisObject.setMoveImage(null);
        redisUtil.set(DictionaryConst.UserRedisHeader.VERIFICATION_CODE + verificationMode.getId(), redisObject, 60);
        verificationMode.setX(null);
        verificationMode.setY(null);
        return ResultFul.ok(verificationMode);
    }

    @PostMapping(value = "checkGenerateCode")
    private ResultFul<String> checkGenerateCode(@RequestBody VerificationMode mode) {
        Assertion.isNull("请输入验证码", mode, mode.getX(), mode.getY());
        BusinessAssertion.isTrue("验证码不合法", !verificationUtil.checkGenerateCode(mode, false));
        return ResultFul.ok("success");
    }

}
