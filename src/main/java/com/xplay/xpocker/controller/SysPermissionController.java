package com.xplay.xpocker.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mr.wan
 * @since 2021-04-22
 */
@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController {

}
