package com.xplay.xpocker.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xplay.xpocker.business.ResultFul;
import com.xplay.xpocker.config.security.SecurityUtils;
import com.xplay.xpocker.entity.RoomInfoVO;
import com.xplay.xpocker.entity.room.ERoomHistory;
import com.xplay.xpocker.mapper.CrRoomInfoMapper;
import com.xplay.xpocker.service.mahjong.IRoomHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wanjie
 * @date 2021/6/3 16:46
 */
@RestController
@RequestMapping(value = "/history", produces = "application/json;charset=UTF-8")
public class RecordController {
    @Autowired
    @Resource
    private IRoomHistoryService roomHistoryService;

    @GetMapping(value = "recode")
    ResultFul<IPage<RoomInfoVO>> getRecodeList(Page pageParam) {
        IPage<RoomInfoVO> iPage = roomHistoryService.queryUserHistoryRoom(pageParam, SecurityUtils.getUserDetails().getId());
        return ResultFul.ok(iPage);
    }
}
