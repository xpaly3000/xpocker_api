package com.xplay.xpocker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Author: wanjie
 * @Date: 2019/5/5 16:40
 * @Version 1.0
 */
@RestController
@RequestMapping(method = RequestMethod.GET, value = "open/api/app")
public class AppUrlController {
    @GetMapping("dowinfo")
    public ModelAndView getView(){
        ModelAndView modelAndView=new ModelAndView("views/app/dowapp");
        return modelAndView;
    }
}
