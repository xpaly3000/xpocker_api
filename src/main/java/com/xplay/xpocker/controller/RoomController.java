package com.xplay.xpocker.controller;

import com.xplay.xpocker.business.BusinessEnum;
import com.xplay.xpocker.business.ResultFul;
import com.xplay.xpocker.config.security.SecurityUtils;
import com.xplay.xpocker.entity.CrRoomInfo;
import com.xplay.xpocker.meta.room.MetaRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongRoomEntity;
import com.xplay.xpocker.meta.realize.MahjongRule;
import com.xplay.xpocker.meta.realize.MahjongUserChannel;
import com.xplay.xpocker.service.mahjong.IRoomService;
import com.xplay.xpocker.service.system.ICrRoomInfoService;
import com.xplay.xpocker.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wanjie
 * @date 2021/3/17 20:36
 */
@RestController
@RequestMapping(value = "/room", produces = "application/json;charset=UTF-8")
public class RoomController {
    private static final Logger logger = LoggerFactory.getLogger(RoomController.class);

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    @Qualifier(value = "roomActionLock")
    private ConcurrentHashMap<String, ReentrantLock> roomActionLock;
    @Autowired
    private IRoomService roomService;
    @Autowired
    private ICrRoomInfoService crRoomInfoService;

    @PostMapping(value = "create")
    public ResultFul<MetaRoomEntity> createRoom(MetaRoomEntity roomEntity) {
        Integer roomCode = (int) ((Math.random() * 9 + 1) * 100000);
        logger.info(String.format("====================== room [%s] =================", roomCode.toString()));
        roomActionLock.put(roomCode.toString(), new ReentrantLock());
        CrRoomInfo crRoomInfo = new CrRoomInfo();
        crRoomInfo.setCode("CREATE_ROOM");
        crRoomInfo.setName("创建房间");
        crRoomInfo.setCreater(SecurityUtils.getUserDetails().getUsername());
        crRoomInfo.setRoomCode(roomCode.toString());
        crRoomInfo.setCreateDate(LocalDateTime.now());
        crRoomInfo.setRoomType(roomEntity.getRoomType());
        crRoomInfo.setCreateStr(JacksonStringUtil.objectToString(roomEntity, false));
        crRoomInfoService.addCreateRoomInfo(crRoomInfo);
        roomEntity.setDbSequence(crRoomInfo.getId());
        roomEntity.setCode(roomCode.toString());
        redisUtil.set(DictionaryConst.RedisHeader.ROOM_HEADER + roomCode, roomEntity, DictionaryConst.RedisExpireData.ROOM_EXPIRE);
        roomService.addRoom(roomCode.toString(), SecurityUtils.getUserDetails().getUsername());
        return ResultFul.ok(roomEntity);
    }


    @GetMapping(value = "get")
    public ResultFul<MetaRoomEntity> getRoom() {
        MetaRoomEntity roomEntity = roomService.queryUserRoom(SecurityUtils.getUserDetails().getUsername());
        return ResultFul.ok(roomEntity);
    }

    @PostMapping(value = "add")
    public ResultFul<MetaRoomEntity> addRoom(String roomCode) {
        ReentrantLock roomLock = roomActionLock.get(roomCode);
        BusinessAssertion.isNull(BusinessEnum.ROOM_NOT_FOUND, roomLock);
        roomLock.lock();
        // 下面这个方法没有加锁 所以需要使用房间锁
        MetaRoomEntity roomEntity = null;
        try {
            // 下面这个方法没有加锁 所以需要使用房间锁
            roomEntity = roomService.addRoom(roomCode, SecurityUtils.getUserDetails().getUsername());
        } catch (Exception e) {
            throw e;
        } finally {
            roomLock.unlock();
        }
        return ResultFul.ok(roomEntity.getRoomType());
    }

}
