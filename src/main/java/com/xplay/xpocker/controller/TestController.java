package com.xplay.xpocker.controller;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.xplay.xpocker.entity.system.SysUser;
import com.xplay.xpocker.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanjie
 * @date 2021/3/17 20:36
 */
@RestController
@RequestMapping(value = "test", produces = "application/json;charset=UTF-8")
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);


    @Autowired
    private RedisUtil redisUtil;

    @PostMapping(value = "mahjong")
    public ResponseEntity createMahjongRoom() {
        return ResponseEntity.ok("房间创建成功");
    }

}
