package com.xplay.xpocker.observer;

import com.xplay.xpocker.meta.MessageContent;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author wanjie
 * @date 2021/3/22 17:09
 */

public interface Subject {
    /**
     * 增加订阅者
     * @param observer
     */
    public void attach(MetaUserChannel observer);
    /**
     * 删除订阅者
     * @param observer
     */
    public void detach(MetaUserChannel observer);
    /**
     * 通知订阅者更新消息
     */
    public void notifyMessage(MessageContent message, String userId);

    public ChannelHandlerContext getUserChannelByUserId(String userId);

    public ChannelHandlerContext getUserChannelBySeq(Integer seq);
}
