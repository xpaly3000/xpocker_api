package com.xplay.xpocker.observer;

import com.xplay.xpocker.meta.user.MetaUserChannel;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

/**
 * @author wanjie
 * @date 2021/3/22 17:26
 */
@Data
public class UserChannelBuilder {
    private MetaUserChannel userChannel;
    public UserChannelBuilder(){
        userChannel = new MetaUserChannel();
    }
    private UserChannelBuilder setUserChannel(ChannelHandlerContext userCtx){
        userChannel.setUserCtx(userCtx);
        return this;
    }

    public UserChannelBuilder setUserId(String userId) {
        userChannel.setUserId(userId);
        return this;
    }

    public UserChannelBuilder setBanker(boolean banker) {
        userChannel.setBanker(banker);
        return this;
    }

    public UserChannelBuilder setSeq(int seq) {
        userChannel.setSeq(seq);
        return this;
    }

    public UserChannelBuilder setWin(boolean win) {
        userChannel.setWin(win);
        return this;
    }

    public UserChannelBuilder setChannel(ChannelHandlerContext userCtx){
        userChannel.setUserCtx(userCtx);
        return this;
    }

    public UserChannelBuilder setOnline(boolean online) {
        userChannel.setOnline(online);
        return this;
    }
    public MetaUserChannel build(){
        return userChannel;
    }
}
