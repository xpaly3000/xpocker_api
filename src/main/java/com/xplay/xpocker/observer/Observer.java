package com.xplay.xpocker.observer;

import io.netty.channel.ChannelHandlerContext;

/**
 * @author wanjie
 * @date 2021/3/22 17:05
 */


public interface Observer {

    public void notice(String msg);

    public String key();

    public void setChannel(ChannelHandlerContext userCtx);
}
