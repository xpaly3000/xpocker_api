package com.xplay.xpocker.config;

import com.xplay.xpocker.util.DictionaryConst;
import com.xplay.xpocker.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wanjie
 * @date 2021/5/11 15:44
 * <p>
 * 当项目启动的时候需要做些初始化操作
 * 比如: 将所有房间读取出来 添加到房间锁里面
 */

@Component
public class XpockerStartupRunner implements CommandLineRunner {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    @Qualifier(value = "roomActionLock")
    ConcurrentHashMap<String, ReentrantLock> roomActionLock;


    @Override
    public void run(String... args) throws Exception {
        // 每一个房间都添加一个锁 到集合中
        Set<String> allRoom = redisUtil.getKeys(DictionaryConst.RedisHeader.ROOM_HEADER + "*");
        if (null != allRoom) {
            for (String roomHeader : allRoom) {
                roomActionLock.put(roomHeader.replace(DictionaryConst.RedisHeader.ROOM_HEADER, ""), new ReentrantLock());
            }
        }

    }
}
