package com.xplay.xpocker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wanjie
 * @date 2021/5/2 14:22
 */
@Component
public class LockConfig {


    /**
     * 所有使用到 房间锁的 类  都需要 try cache   避免单次锁过后   导致死锁
     * @return
     */

    @Bean
    public ConcurrentHashMap<String, ReentrantLock> roomActionLock() {
        ConcurrentHashMap<String, ReentrantLock> roomLock = new ConcurrentHashMap<>();
        return roomLock;
    }
}
