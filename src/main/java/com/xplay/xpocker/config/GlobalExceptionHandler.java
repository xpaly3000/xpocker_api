package com.xplay.xpocker.config;

import com.xplay.xpocker.business.BusinessException;
import com.xplay.xpocker.business.ResultFul;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * @author mr.wan
 * @date 2018-11-23
 */
@Slf4j
@RestControllerAdvice
@CrossOrigin
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    public ResultFul businessException(BusinessException exception) {
        exception.printStackTrace();
        return ResultFul.businessException(exception);
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity runtimeException(RuntimeException exception) {
        exception.printStackTrace();
        return ResponseEntity.status(500).body(exception.getMessage());
    }


}
