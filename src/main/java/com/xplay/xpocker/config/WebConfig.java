package com.xplay.xpocker.config;

import com.xplay.xpocker.interceptor.UserLoginInterceptor;
import com.xplay.xpocker.util.DictionaryUrlConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author wanjie
 * @date 2021/3/22 15:23
 */

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private UserLoginInterceptor userLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userLoginInterceptor).addPathPatterns("/xpocker/**").excludePathPatterns(DictionaryUrlConst.ControllerHeader.sys + DictionaryUrlConst.UserController.login).excludePathPatterns(DictionaryUrlConst.ControllerHeader.sys + DictionaryUrlConst.UserController.create);
    }
}
