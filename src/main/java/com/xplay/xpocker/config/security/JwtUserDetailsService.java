package com.xplay.xpocker.config.security;


import com.xplay.xpocker.entity.system.SysUser;
import com.xplay.xpocker.entity.system.SysUserDTO;
import com.xplay.xpocker.service.system.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author mr.wan
 * @date 2018-11-22
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private ISysUserService userService;


    @Override
    public JwtUser loadUserByUsername(String username) {
        SysUserDTO userDTO = userService.queryUserInfoByUserName(username);
        return buildUser(userDTO);
    }

    public JwtUser buildUser(SysUserDTO userDTO) {
        SysUser sysUser = userDTO.getSysUser();
        return new JwtUser(sysUser.getId(), sysUser.getUsername(),
                sysUser.getPassword(), sysUser.getPortraitUrl(), sysUser.getEmail(), sysUser.getPhone(),
                userDTO.getGrantedAuthorities(), true, sysUser.getCreateDate(), null);
    }

}
