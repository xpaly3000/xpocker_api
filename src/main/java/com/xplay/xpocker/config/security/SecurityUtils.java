package com.xplay.xpocker.config.security;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * 获取当前登录的用户
 *
 * @author mr.wan
 * @date 2019-01-17
 */
public class SecurityUtils {

    public static JwtUser getUserDetails() {
        JwtUser userDetails = null;
        try {
            userDetails = (JwtUser) org.springframework.security.core.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new RuntimeException("登录状态过期");
        }
        return userDetails;
    }

}
