package com.xplay.xpocker;

import com.xplay.xpocker.socket.WSServer;
import com.xplay.xpocker.util.SpringContextHolder;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * ConnectStrategy  玩家连接处理器
 * MessageStrategy  玩家消息处理器
 * <p>
 * 两个处理器 都是策略模式 将元数据 MetaRoomEntity 贯穿代码块
 * 通过redis 存储房间对象  和 ReentrantLock 保证数据线程安全
 * 在内部请不要做任何的 save
 */

@SpringBootApplication
@MapperScan("com.xplay.xpocker.mapper")
public class XpockerApplication extends SpringBootServletInitializer implements CommandLineRunner {
    @Autowired
    private WSServer wsServer;

    public static void main(String[] args) {
        SpringApplication.run(XpockerApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(XpockerApplication.class);
    }

    @Bean
    public SpringContextHolder springContextHolder() {
        return new SpringContextHolder();
    }

    @Override
    public void run(String... args) throws Exception {
        wsServer.start();
    }
}
