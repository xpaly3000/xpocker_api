package com.xplay.xpocker.meta;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xplay.xpocker.message.MessageToEnum;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import lombok.Data;

/**
 * @author wanjie
 * @date 2021/3/17 22:00
 */

@Data
public class MessageContent<T> {
    private T body;
    private String action;
    private String roomCode;
    private MetaUserChannel userChannel;
    private String userId;
    private String code;
    private MessageToEnum messageType;
    private String tips;

    public void cleanBody() {
        this.body = null;
    }

    public MessageContent() {
    }

    public MessageContent(String action) {
        this.action = action;
    }

    public MessageContent(T body, String action) {
        this.body = body;
        this.action = action;
    }

    public MessageContent initActionAndUId(String action, String userId) {
        this.action = action;
        this.userId = userId;
        return this;
    }

    public MessageContent(T body, String action, String userId) {
        this.body = body;
        this.action = action;
        this.userId = userId;
    }

    public MessageContent(T body, String action, String userId, String code) {
        this.body = body;
        this.action = action;
        this.code = code;
    }

    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void cleanMessageType() {
        this.messageType = null;
    }
}
