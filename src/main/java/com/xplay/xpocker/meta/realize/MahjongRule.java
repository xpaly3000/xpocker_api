package com.xplay.xpocker.meta.realize;

import com.xplay.xpocker.meta.rule.MetaRoomRule;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wanjie
 * @date 2021/4/26 16:31
 */
@Data
public class MahjongRule extends MetaRoomRule {
    int numberGame;
    Integer turns;
    List<String> rules;

    public List<String> getRules() {
        if (this.rules == null) {
            return new ArrayList<>();
        }
        return rules;
    }


}
