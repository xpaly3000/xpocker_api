package com.xplay.xpocker.meta.realize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.xplay.xpocker.entity.mahjong.UserSwapCard;
import com.xplay.xpocker.entity.system.SysUser;
import com.xplay.xpocker.meta.user.MetaUserChannel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.Data;

import java.util.ArrayList;

/**
 * @author wanjie
 * @date 2021/3/22 17:05
 * <p>
 * 此 UserChannel 只能存放基本信息 需要用于网络中传输
 */
@Data
public class MahjongUserChannel extends MetaUserChannel {
    private SysUser sysUser;
    // 用户标识
    private String userId;
    // 用户的消息通道
    @JsonIgnore
    private ChannelHandlerContext userCtx;

    // 胡牌的数据
    private Integer huCard;

    private int integral;

    private boolean prepare;

    private boolean swap;

    // 房间状态
    private int gameType;

    private boolean listening;

    private String firingUser;

    // 一共胡了多少次
    private int winCount;
    // 报叫后 胡哪些牌   过敏信息
    private ArrayList<Integer> callHuCards;

    // 换牌信息  过敏信息
    private UserSwapCard userSwapCard;

    public MahjongUserChannel() {
    }

    @Override
    public void setChannel(ChannelHandlerContext userCtx) {
        this.userCtx = userCtx;
    }

    public MahjongUserChannel(String userId, ChannelHandlerContext userCtx, boolean banker, int seq, boolean win, boolean online, boolean homeowners) {
        this.userId = userId;
        this.userCtx = userCtx;
        this.banker = banker;
        this.seq = seq;
        super.win = win;
        super.online = online;
        this.homeowners = homeowners;
    }

    @Override
    public void notice(String msg) {
        userCtx.channel().write(new TextWebSocketFrame(msg));
    }

    @Override
    public String key() {
        return userId;
    }

}
