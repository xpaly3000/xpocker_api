package com.xplay.xpocker.meta.rule;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/4/30 8:40
 */
@Data
public class MetaRoomRule {
   public int userCount;
}
