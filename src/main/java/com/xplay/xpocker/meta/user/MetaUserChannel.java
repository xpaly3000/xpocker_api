package com.xplay.xpocker.meta.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.xplay.xpocker.entity.system.SysUser;
import com.xplay.xpocker.observer.Observer;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wanjie
 * @date 2021/3/22 17:05
 * <p>
 * 此 UserChannel 只能存放基本信息 需要用于网络中传输
 */
@Data
public class MetaUserChannel implements Observer, Serializable {

    private static final long serialVersionUID = -1368285358147253569L;

    protected SysUser sysUser;
    // 用户标识
    protected String userId;
    // 用户的消息通道
    @JsonIgnore
    private ChannelHandlerContext userCtx;
    // 是否为庄家
    protected boolean banker;

    // 用户的序列编号
    protected int seq;

    // 是否已经胡牌
    protected boolean win;

    // 是否已经掉线
    protected boolean online;

    // 是否为第一次连接 默认为 false
    protected boolean firstConnection;

    // 是否为房主
    protected boolean homeowners;


    public MetaUserChannel() {
    }

    @Override
    public void setChannel(ChannelHandlerContext userCtx) {
        this.userCtx = userCtx;
    }

    public MetaUserChannel(String userId, ChannelHandlerContext userCtx, boolean banker, int seq, boolean win, boolean online,boolean homeowners) {
        this.userId = userId;
        this.userCtx = userCtx;
        this.banker = banker;
        this.seq = seq;
        this.win = win;
        this.online = online;
        this.homeowners = homeowners;
    }

    @Override
    public void notice(String msg) {
        userCtx.channel().write(new TextWebSocketFrame(msg));
    }

    @Override
    public String key() {
        return userId;
    }

}
