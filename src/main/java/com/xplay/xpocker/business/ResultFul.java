package com.xplay.xpocker.business;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/4/22 20:16
 */

@Data
public class ResultFul<T> {
    public static final String SUCCESS_CODE = "0000";
    public static final String ERROR_CODE = "8888";
    private String code;
    private String msg;
    private T body;

    ResultFul(String code, String msg, T body) {
        this.code = code;
        this.msg = msg;
        this.body = body;
    }

    ResultFul(String code) {
        this.code = code;
    }

    ResultFul(String code, T t) {
        this.code = code;
        this.body = t;
    }

    ResultFul(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static <T> ResultFul ok(T t) {
        return new ResultFul(SUCCESS_CODE, t);
    }

    public static <T> ResultFul fail() {
        return new ResultFul(ERROR_CODE);
    }

    public static <T> ResultFul fail(String message) {
        return new ResultFul(ERROR_CODE, message);
    }

    public static ResultFul businessEnum(BusinessEnum businessEnum) {
        return new ResultFul(businessEnum.getCode(), businessEnum.getMsg());
    }

    public static ResultFul businessException(BusinessException exception) {
        return new ResultFul(exception.getStateCode(), exception.getMessage());
    }
}
