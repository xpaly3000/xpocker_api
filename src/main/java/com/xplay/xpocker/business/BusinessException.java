package com.xplay.xpocker.business;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/4/20 20:31
 */
@Data
public class BusinessException extends RuntimeException {
    private String stateCode;
    private String message;

    public BusinessException(String message) {
        super(message);
        this.stateCode = ResultFul.ERROR_CODE;
        this.message = message;
    }

    public BusinessException(BusinessEnum businessEnum) {
        super(businessEnum.getMsg());
        this.stateCode = businessEnum.getCode();
        this.message = businessEnum.getMsg();
    }

    public BusinessException(String message, String stateCode) {
        super(message);
        this.message = message;
        this.stateCode = stateCode;
    }
}
