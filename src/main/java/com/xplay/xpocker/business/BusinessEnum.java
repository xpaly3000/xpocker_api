package com.xplay.xpocker.business;

import lombok.Data;

/**
 * @author wanjie
 * @date 2021/4/22 20:14
 */
public enum BusinessEnum {
    /***
     * 200001  app 做了 游戏页面退出
     */
    SUCCESS("1111", "成功"), ERROR("8888", "失败"), VALIDATE_CODE_ERROR("100001", "验证码错误"),
    ROOM_NOT_FOUND("200001", "房间不存在"),
    ROOM_ON_MORE("200001", "多次进入房间"),
    ROOM_FULL("200002", "房间已满"),
    UNAUTHORIZED("401", "用户未认证");

    private String code;
    private String msg;

    BusinessEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
